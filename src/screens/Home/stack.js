import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import Home from './index';
import PaymentDetails from './paymentDetails';
import ShipmentDetails from './shipmentDetails';
import Success from './success';


const Stack = createNativeStackNavigator();
const headerOptions = {
    headerShown: false,
};
export const HomeStack = () => {
    return (
        <Stack.Navigator screenOptions={headerOptions}>
            <Stack.Screen
                name="Home"
                component={Home}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="payment"
                component={PaymentDetails}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="shipment"
                component={ShipmentDetails}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="success"
                component={Success}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};

