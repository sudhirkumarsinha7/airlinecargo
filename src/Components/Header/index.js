/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import React, { Component } from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { localImage } from '../../config/global';
import { DeviceHeight, DeviceWidth, Colors } from '../Common/Style';
import { moderateScale, scale, verticalScale } from '../Scale';
import { useTheme } from '../../theme/Hooks'
import fonts from '../../constants/fonts';
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useTranslation } from 'react-i18next';

export const Header = (props) => {
  const insets = useSafeAreaInsets()
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const { t } = useTranslation();

  return (
    <ImageBackground
      source={localImage.backgraound}
      resizeMode="cover"
    // style={styles.imgBackground}
    >
      <View style={{ paddingTop: insets.top + 15, paddingBottom: 15, marginbottom: scale(15) }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 0.15, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => { props.onPressLeft() }}
              style={{}}>
              <Ionicons name="menu-outline" size={30} color={props.textColor} />

            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.7, justifyContent: 'center', alignItems: 'center' }}>

            <Text style={{
              color: props.textColor,
              fontSize: 16,
              textAlign: 'center',
              fontFamily: fonts.BOLD
            }}>{t(props.name)} </Text>
          </View>
          <View style={{ flex: 0.15, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity onPress={() => { props.onPressRight() }}
              style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff', borderRadius: 100 }}>
            </TouchableOpacity>
          </View>

        </View>
      </View>
    </ImageBackground>

  );
}



export const Header1 = (props) => {
  const insets = useSafeAreaInsets()
  const { t } = useTranslation();

  const { isBack = false, isRightIcon = false } = props
  return (<ImageBackground
    source={localImage.backgraound}
    resizeMode="cover"
  // style={styles.imgBackground}
  >
    <View
      style={{
        flexDirection: 'row',
        paddingTop: insets.top + 15,
        paddingBottom: 10,
        width: '100%',
        // height: 50,
      }}>
      <TouchableOpacity onPress={() => isBack ? props.navigation.goBack() : {}}
        style={{ width: '15%', alignItems: 'center', justifyContent: 'center' }}>
        {isBack ? <Ionicons name="chevron-back-outline" size={25} color={props.textColor} /> : <></>}
      </TouchableOpacity>
      <View style={{ width: '70%', alignItems: 'center', justifyContent: 'center' }}>
        <Text
          style={{
            fontFamily: fonts.SEMI_BOLD,
            color: props.textColor,
            fontSize: scale(18),
          }}>
          {t(props.name)}
        </Text>
      </View>
      {isRightIcon ? <View style={{ width: '15%' }}>
        <TouchableOpacity onPress={() => { props.onPressRight() }}
          style={{ alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff', borderRadius: 100 }}>
        </TouchableOpacity>
      </View> : null}


    </View>
  </ImageBackground>)
}
