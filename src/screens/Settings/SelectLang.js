// SelectLang.js
import React from 'react';
import { View, Text, Button, TouchableOpacity, FlatList } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useLanguage } from '../../languages/LanguageContext';
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { localImage, langList, defaultlang } from "../../config/global";
import Ionicons from 'react-native-vector-icons/Ionicons'
import fonts from "../../constants/fonts";
import { scale } from "../../Components/Scale";
import { Colors } from "../../Components/Common/Style";
export const LangButton = (props) => {
    const { t } = useTranslation();

    const { title = '', currentLanguage, changeLanguage, id = '', icon = '' } = props;
    return (<TouchableOpacity onPress={() => changeLanguage()} style={{ flexDirection: 'row', backgroundColor: currentLanguage === id ? '#B59DE1' : '#f8f5fc', borderRadius: scale(10), padding: scale(7), alignItems: 'center', margin: scale(10) }}>
        <View style={{ width: scale(40), height: scale(40), borderRadius: scale(20), borderWidth: 1, justifyContent: 'center', padding: scale(5), borderColor: '#B59DE1', backgroundColor: 'white' }}>
            <Text style={{ textAlign: 'center', fontSize: scale(16), fontFamily: fonts.MEDIUM, color: Colors.primary }}>{icon}</Text>
        </View>
        <Text style={{ marginLeft: scale(20), color: '#000', fontSize: scale(16), fontFamily: fonts.REGULAR, }}>
            {t(title)}
        </Text>
    </TouchableOpacity>)
}

const SelectLang = (props) => {
    const { t } = useTranslation();
    const { currentLanguage, changeLanguage } = useLanguage();
    const insets = useSafeAreaInsets()





    return (
        <View style={{ backgroundColor: '#fff', paddingTop: insets.top, flex: 1, }}>
            <TouchableOpacity onPress={() => props.navigation.goBack()}
                style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center', borderColor: '#E5E7EB' }}>
                <Ionicons name="chevron-back-outline" size={25} color={'black'} />
            </TouchableOpacity>
            <View style={{ alignItems: 'center' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, fontSize: scale(20), color: '#090A0A', }}>{t('Choose your language')}</Text>
                <Text style={{ fontFamily: fonts.REGULAR, fontSize: scale(14), color: '#808080', marginTop: scale(10) }}>{t('you can change it later in settings')}</Text>
            </View>
            <LangButton title='English'
                currentLanguage={currentLanguage}
                changeLanguage={() => changeLanguage('en')} id={'en'} icon='A' />
            <LangButton title='Hindi'
                currentLanguage={currentLanguage}
                changeLanguage={() => changeLanguage('hi')} id={'hi'} icon='ह' />

            <LangButton title='Telugu'
                currentLanguage={currentLanguage}
                changeLanguage={() => changeLanguage('te')} id={'te'} icon='అ' />
        </View>
    );
};

export default SelectLang;