import React, { useRef, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Alert,
    TouchableOpacity,
    StatusBar,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { CustomTextView } from '../../Components/Common/heper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { scale } from '../../Components/Scale';
import fonts from '../../constants/fonts';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Colors } from '../../Components/Common/Style';
import { CustomInputText, SubHeaderComponent, CustomTextLine } from "../../Components/Common/heper"
import { Header1 } from '../../Components/Header'
import { CustomButton, CustomButtonLine } from '../../Components/Common/Button';
import AntDesign from 'react-native-vector-icons/AntDesign';
const Home = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const insets = useSafeAreaInsets();

    return (
        <View
            style={{
                flex: 1,

            }}>
            <StatusBar
                hidden={false}
                backgroundColor={'#fff'}
                barStyle={'dark-content'}
            />
            <Header1 isBack={true} name={'Booking Confirmed'} navigation={props.navigation} textColor={'#fff'} />

            <View style={{ backgroundColor: Colors.whiteFF, margin: 15, padding: 15, borderRadius: 10 }}>
                <View style={{ alignSelf: 'center', margin: scale(30) }}>
                    <AntDesign name={'checkcircle'} size={100} color={'#53D674'} />

                </View>
                <Text style={styles.title}>{'Payment Done Successfully'}</Text>

                <Text style={styles.subTitle}>{'Your Booking is confirmed and QR code is sent to your registered mobile number'}</Text>
                <View style={{ alignSelf: 'center', margin: scale(30) }}>
                    <Ionicons name={'qr-code'} size={200} color={'#000'} />

                </View>

            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: { flex: 1, paddingHorizontal: 17, paddingVertical: 10 },
    title: { fontSize: scale(20), fontFamily: fonts.SEMI_BOLD, color: '#090A0A' },
    subTitle: {
        fontSize: scale(13),
        fontFamily: fonts.REGULAR,
        color: '#888889',
        marginVertical: 10,
    },
    phoneInputContainer: {
        width: '100%',
        overflow: 'visible',
        borderWidth: 1,
        borderRadius: 1,
        overflow: 'visible',
        borderColor: '#a8a8a8',
        // marginBottom: 5,
    },
    phoneInputTextContainer: {
        // overflow: 'visible',
        backgroundColor: '#FFFFFF',
    },
    phoneInputText: { padding: 0 },
});
export default Home;
