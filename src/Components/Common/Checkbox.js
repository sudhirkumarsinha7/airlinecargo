import {View, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {scale} from '../Scale';
import fonts from '../../constants/fonts';
import {useTranslation} from 'react-i18next';
import {Colors} from './Style';

const Checkbox = ({
  isChecked,
  toggleCheckbox,
  title,
  containerStyle,
  textStyle,
}) => {
  const {t, i18n} = useTranslation();
  return (
    <TouchableOpacity style={containerStyle} onPress={() => toggleCheckbox(!isChecked)}>
      <Ionicons
        name={isChecked ? 'checkbox' : 'checkbox-outline'}
        size={22}
        color={isChecked ? Colors.primary : '#dcdcdc'}/>
      <Text style={textStyle}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Checkbox;
