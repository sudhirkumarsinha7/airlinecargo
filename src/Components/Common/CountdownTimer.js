/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import { useCountdown } from '../../hooks/useCountdown';
import { Colors } from './Style';
import { scale } from './Style';
export const CountdownTimer = ({ targetDate }) => {
  const [days, hours, minutes, seconds] = useCountdown(targetDate);

  if (days + hours + minutes + seconds <= 0) {
    return <ShowCounter
      days={'00'}
      hours={'00'}
      minutes={'00'}
      seconds={'00'}
    />;
  } else {
    return (
      <ShowCounter
        days={days}
        hours={hours}
        minutes={minutes}
        seconds={seconds}
      />
    );
  }
};
const ExpiredNotice = () => {
  return (
    <View>
      <Text>{'00:00:00:00'}</Text>
    </View>
  );
};
const ShowCounter = ({ days, hours, minutes, seconds }) => {
  const timerDesig = (val) => {
    return <View style={{ flexDirection: 'row', backgroundColor: '#302F2F', padding: scale(2), paddingLeft: scale(6), paddingRight: scale(6), borderRadius: scale(2) }}>
      <Text style={{ color: Colors.whiteFF }}>{val + ''}</Text>
    </View>
  }
  return (
    <View style={{ flexDirection: 'row',backgroundColor: Colors.whiteFF }}>
      {timerDesig(days)}
      <Text style={{ color: '#302F2F',fontSize:18,textAlign:'center' }}>{':'}</Text>

      {timerDesig(hours)}
      <Text style={{ color: '#302F2F',fontSize:18,textAlign:'center' }}>{':'}</Text>

      {timerDesig(minutes)}
      <Text style={{color: '#302F2F',fontSize:18,textAlign:'center' }}>{':'}</Text>

      {timerDesig(seconds)}
    </View>
  );
};