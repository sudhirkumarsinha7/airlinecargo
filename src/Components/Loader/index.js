import React from 'react';
import {View, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import Svg, {Defs, LinearGradient, Stop, Rect} from 'react-native-svg';
import {MotiView} from 'moti';

const Gradient = () => (
  <Svg viewBox="0 0 100 100">
    <Defs>
      <LinearGradient id={'gradient'} x1={'0%'} y1={'0%'} x2={'100%'} y2={'0%'}>
        <Stop stopOpacity={0} stopColor={'rgb(225, 225, 225)'} offset={'0%'} />
        <Stop
          stopOpacity={0.0112304688}
          stopColor={'rgb(225, 225, 225)'}
          offset={'0.330810545%'}
        />
        <Stop
          stopOpacity={0.04296875}
          stopColor={'rgb(224, 224, 224)'}
          offset={'1.287109375%'}
        />
        <Stop
          stopOpacity={0.0922851563}
          stopColor={'rgb(223, 223, 223)'}
          offset={'2.814697265%'}
        />
        <Stop
          stopOpacity={0.15625}
          stopColor={'rgb(221, 221, 221)'}
          offset={'4.859375%'}
        />
        <Stop
          stopOpacity={0.2319335938}
          stopColor={'rgb(219, 219, 219)'}
          offset={'7.3669433600000005%'}
        />
        <Stop
          stopOpacity={0.31640625}
          stopColor={'rgb(217, 217, 217)'}
          offset={'10.283203125%'}
        />
        <Stop
          stopOpacity={0.4067382813}
          stopColor={'rgb(215, 215, 215)'}
          offset={'13.55395508%'}
        />
        <Stop
          stopOpacity={0.5}
          stopColor={'rgb(213, 213, 213)'}
          offset={'17.125%'}
        />
        <Stop
          stopOpacity={0.5932617188}
          stopColor={'rgb(210, 210, 210)'}
          offset={'20.94213867%'}
        />
        <Stop
          stopOpacity={0.68359375}
          stopColor={'rgb(208, 208, 208)'}
          offset={'24.951171875%'}
        />
        <Stop
          stopOpacity={0.7680664063}
          stopColor={'rgb(206, 206, 206)'}
          offset={'29.09790039%'}
        />
        <Stop
          stopOpacity={0.84375}
          stopColor={'rgb(204, 204, 204)'}
          offset={'33.328125%'}
        />
        <Stop
          stopOpacity={0.9077148438}
          stopColor={'rgb(202, 202, 202)'}
          offset={'37.587646485%'}
        />
        <Stop
          stopOpacity={0.95703125}
          stopColor={'rgb(201, 201, 201)'}
          offset={'41.822265625%'}
        />
        <Stop
          stopOpacity={0.9887695313}
          stopColor={'rgb(200, 200, 200)'}
          offset={'45.977783205%'}
        />
        <Stop stopOpacity={1} stopColor={'rgb(200, 200, 200)'} offset={'50%'} />
        <Stop
          stopOpacity={0.9887695313}
          stopColor={'rgb(200, 200, 200)'}
          offset={'54.022216795%'}
        />
        <Stop
          stopOpacity={0.95703125}
          stopColor={'rgb(201, 201, 201)'}
          offset={'58.177734375%'}
        />
        <Stop
          stopOpacity={0.9077148438}
          stopColor={'rgb(202, 202, 202)'}
          offset={'62.412353515%'}
        />
        <Stop
          stopOpacity={0.84375}
          stopColor={'rgb(204, 204, 204)'}
          offset={'66.671875%'}
        />
        <Stop
          stopOpacity={0.7680664063}
          stopColor={'rgb(206, 206, 206)'}
          offset={'70.90209961%'}
        />
        <Stop
          stopOpacity={0.68359375}
          stopColor={'rgb(208, 208, 208)'}
          offset={'75.048828125%'}
        />
        <Stop
          stopOpacity={0.5932617188}
          stopColor={'rgb(210, 210, 210)'}
          offset={'79.05786133000001%'}
        />
        <Stop
          stopOpacity={0.5}
          stopColor={'rgb(213, 213, 213)'}
          offset={'82.875%'}
        />
        <Stop
          stopOpacity={0.4067382813}
          stopColor={'rgb(215, 215, 215)'}
          offset={'86.44604492%'}
        />
        <Stop
          stopOpacity={0.31640625}
          stopColor={'rgb(217, 217, 217)'}
          offset={'89.716796875%'}
        />
        <Stop
          stopOpacity={0.2319335938}
          stopColor={'rgb(219, 219, 219)'}
          offset={'92.63305664%'}
        />
        <Stop
          stopOpacity={0.15625}
          stopColor={'rgb(221, 221, 221)'}
          offset={'95.140625%'}
        />
        <Stop
          stopOpacity={0.0922851563}
          stopColor={'rgb(223, 223, 223)'}
          offset={'97.185302735%'}
        />
        <Stop
          stopOpacity={0.04296875}
          stopColor={'rgb(224, 224, 224)'}
          offset={'98.712890625%'}
        />
        <Stop
          stopOpacity={0.0112304688}
          stopColor={'rgb(225, 225, 225)'}
          offset={'99.669189455%'}
        />
        <Stop
          stopOpacity={0}
          stopColor={'rgb(225, 225, 225)'}
          offset={'100%'}
        />
      </LinearGradient>
    </Defs>
    <Rect width={'50%'} height={'100%'} fill="url(#gradient)" />
  </Svg>
);

const AnimatedGradient = () => (
  <MotiView
    style={{
      width: 500,
      height: 500,
      opacity: 0.5,
    }}
    from={{
      translateX: -300,
    }}
    animate={{
      translateX: 300,
    }}
    transition={{
      loop: true,
      repeatReverse: false,
      type: 'timing',
      duration: 800,
    }}>
    <Gradient />
  </MotiView>
);

const MotiPlaceholders = () => {
  const background = 225;
  const backgroundColor = `rgb(${background}, ${background}, ${background})`;

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView style={{flex: 1, flexDirection: 'column'}}>
        {Array.from({length: 1}).map((_, index) => (
          <View key={index} style={{flex: 1, margin: 10, marginBottom: 20}}>
            <View
              style={{
                flexDirection: 'row',
                marginBottom: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {/* Circle */}
              {/* <View
                style={{
                  height: 100,
                  overflow: 'hidden',
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View
                  style={{
                    height: 100,
                    width: 100,
                    backgroundColor: backgroundColor,
                    overflow: 'hidden',
                    borderRadius: 50,
                  }}>
                  <AnimatedGradient />
                </View>
              </View> */}

              <View style={{flex: 2}}>
                {Array.from({length: 1}).map((_, index) => (
                  <View
                    key={index}
                    style={{
                      height: index === 0 ? 10 : 25,
                      width: index === 0 ? 100 : undefined,
                      backgroundColor: backgroundColor,
                      overflow: 'hidden',
                      borderRadius: 5,
                      marginBottom: 10,
                    }}>
                    <AnimatedGradient />
                  </View>
                ))}
              </View>
            </View>

            {Array.from({length: 1}).map((_, index) => (
              <View key={index}>
                <View
                  style={{
                    height: 50,
                    backgroundColor: backgroundColor,
                    marginBottom: 10,
                    borderRadius: 10,
                    overflow: 'hidden',
                  }}>
                  <AnimatedGradient />
                </View>
              </View>
            ))}
          </View>
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

export default MotiPlaceholders;