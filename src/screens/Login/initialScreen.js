import React, { useRef, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Alert,
  TouchableOpacity,
  StatusBar,
  ImageBackground
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import PhoneInput from 'react-native-phone-number-input';
import { CustomButton } from '../../Components/Common/Button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { scale } from '../../Components/Scale';
import fonts from '../../constants/fonts';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Colors } from '../../Components/Common/Style';
import { DeviceWidth, localImage } from '../../config/global';
import { SubHeaderComponent } from '../../Components/Common/heper'
const InitialScreen = props => {
  const { t, i18n } = useTranslation();
  const isFocused = useIsFocused();
  const insets = useSafeAreaInsets();



  return (
    <ImageBackground
      source={localImage.backgraound}
      resizeMode="cover"
      style={styles.imgBackground}
    >
      <View
        style={{
          flex: 1,
          paddingTop: insets.top + 20,
          paddingBottom: insets.bottom,
        }}>
        <StatusBar
          hidden={false}
          backgroundColor={'#fff'}
          barStyle={'dark-content'}
        />
        <View style={{ marginTop: DeviceWidth / 2 }} />
        <CustomButton
          bgColor={'#FF6A28'}
          buttonName={t('Book Airline')}
          navigation={props.navigation}
          onPressButton={() => props.navigation.navigate('Login')}
        />
        <View style={{ marginTop: DeviceWidth / 5, justifyContent: 'center', alignItems: 'center' }} >

          <SubHeaderComponent
            name='Airline Portal'
            textColor='#fff' />
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, paddingHorizontal: 17, paddingVertical: 10 },
  title: { fontSize: scale(20), fontFamily: fonts.SEMI_BOLD, color: '#090A0A' },
  subTitle: {
    fontSize: scale(13),
    fontFamily: fonts.REGULAR,
    color: '#888889',
    marginVertical: 10,
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
});
export default InitialScreen;
