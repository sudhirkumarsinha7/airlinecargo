import {
  Dimensions,
} from 'react-native';
export const DeviceHeight = Dimensions.get('window').height;
export const DeviceWidth = Dimensions.get('window').width;
export const UserProf = {
  name: 'John Doe',
  company: 'Far N Par (India) Private Limited',
  pdaAmount: '27,76,000'
}
export const MAP_BOX_ACCESS_TOKEN = 'sk.eyJ1Ijoic3VkaGlyOTQiLCJhIjoiY2xvd3J3azd3MTdhdTJpbGU0MDdmbW9haiJ9.XQ0VaQG1dhhaf6aWNEAQnw';
