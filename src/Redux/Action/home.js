/* eslint-disable functional/no-let */
/* eslint-disable sort-keys */
import axios from 'axios';

import { config, getIosDeviceTokeApi, auth } from '../../config/config';
import {
  ETH_TO_USD,
  SWAPCRYPTO,
  BUYCRYPTO,
  SELLCRYPTO,
  CURRENCY_LIST
} from '../constant';

import { loadingOff, loadingOn } from './loader';

// import AsyncStorage from '@react-native-async-storage/async-storage';
export const uploadImage =
  (data) =>
    async (dispatch, getState) => {
      loadingOn(dispatch);
      try {
        let response;
        const configs = {
          headers: {
            'content-type': 'multipart/form-data',
            'authorization': 'Basic MkRUZjJLMWY5Q0hiWERwMDR2dG1nMDZNTEt6OjcxMWQyOTBiNDlmZmU2ZTBmMTE5NmY5ODBhNTI0ODlh'
          },
        };
        console.log(
          'Action_uuploadImagesUrl ' + JSON.stringify(`${config().uploadImage}`),
        );
        console.log('Action_upload_Image ', data);
        response = await axios.post(config().uploadImage, data, configs);
        console.log('Action_upload_Image_response' + JSON.stringify(response));
        return response;
      } catch (e) {
        console.log('Action_upload_Image_error' + JSON.stringify(e));
      } finally {
        loadingOff(dispatch);
      }
    };
export const ethToUSDConvertor = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {

    const response = await axios.get(config().ethtousdconvert);
    console.log('ActionethToUSDConvertor_response' + JSON.stringify(response));
    dispatch({
      type: ETH_TO_USD,
      payload: response.data,
    });

    return response;
  } catch (e) {
    console.log('Action_ethToUSDConvertor_error' + JSON.stringify(e));
  } finally {
    loadingOff(dispatch);
  }
};
export const getSwapTransaction = (skip, limit, email) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log(
      'Action_getSwapTransactionl ' +
      JSON.stringify(`${config().getTransactions}`),
    );
    const response = await axios.get(
      `${config().getTransactions}?skip=${skip}&limit=${limit}&user=${email}`,
    );

    console.log('Action_getSwapTransactionl ' + JSON.stringify(response));

    if (response.status === 200) {
      dispatch({
        type: SWAPCRYPTO,
        payload: response.data.data,
      });
    }


    return response;
  } catch (e) {
    console.log('Action_getSwapTransaction_error', e.response);
    //   
  } finally {
    loadingOff(dispatch);
  }
};
export const getBuyTransaction = (skip, limit, email) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log(
      'Action_getBuyTransaction ' +
      JSON.stringify(`${config().buyTrans}`),
    );
    const response = await axios.get(
      `${config().buyTrans}?skip=${skip}&limit=${limit}&user=${email}`,
    );
    console.log('Action_getBuyTransaction ' + JSON.stringify(response));

    if (response.status === 200) {
      dispatch({
        type: BUYCRYPTO,
        payload: response.data.data,
      });
    }


    return response;
  } catch (e) {
    console.log('Action_getBuyTransaction_error', e.response);
    //   
  } finally {
    loadingOff(dispatch);
  }
};

export const getSellTransaction = (skip, limit, email) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log(
      'Action_getSellTransaction ' +
      JSON.stringify(`${config().sellTrans}`),
    );
    const response = await axios.get(
      `${config().sellTrans}?skip=${skip}&limit=${limit}&user=${email}`,
    );
    console.log('Action_getSellTransaction ' + JSON.stringify(response));

    if (response.status === 200) {
      dispatch({
        type: SELLCRYPTO,
        payload: response.data.data,
      });
    }


    return response;
  } catch (e) {
    console.log('Action_getSellTransaction_error', e.response);
    //   
  } finally {
    loadingOff(dispatch);
  }
};
export const getCurrencyList = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    console.log(
      'Action_getCurrencyList' +
      JSON.stringify(`${config().getCurrencyList}`),
    );
    const response = await axios.get(
      `${config().getCurrencyList}`,
    );

    console.log('Action_getCurrencyList ' + JSON.stringify(response));

    if (response.status === 200) {
      dispatch({
        type: CURRENCY_LIST,
        payload: response.data.data,
      });
    }


    return response;
  } catch (e) {
    console.log('Action_getCurrencyList_error', e.response);
    //   
  } finally {
    loadingOff(dispatch);
  }
};
export const resetTransaction = () => async (dispatch, getState) => {
  dispatch({
    type: SWAPCRYPTO,
    payload: [],
  });
  dispatch({
    type: BUYCRYPTO,
    payload: [],
  });
  dispatch({
    type: SELLCRYPTO,
    payload: [],
  });
};

export const getConversionRate = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_sgetConversionRate_data' + JSON.stringify(data));
    let response = await axios.post(config().getConversionRate, data);

    console.log('Action_getConversionRater_response' + JSON.stringify(response));
    return response.data;
  } catch (e) {
    console.log('Action_getConversionRater_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};

export const validateAddresss = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_validateAddresss_data' + JSON.stringify(data));
    let response = await axios.post(config().validateAddresss, data);

    console.log('Action_validateAddresss_response' + JSON.stringify(response));
    return response.data;
  } catch (e) {
    console.log('Action_validateAddresss_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};




export const createSwap = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_createSwap_data' + JSON.stringify(data));
    let response = await axios.post(config().createSwap, data);

    console.log('Action_createSwap_response' + JSON.stringify(response));
    return response.data;
  } catch (e) {
    console.log('Action_createSwap_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const createSwapTransaction = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_createSwapTransaction_data' + JSON.stringify(data));
    let response = await axios.post(config().swapTransaction, data);

    console.log('Action_createSwapTransaction_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_createSwapTransaction_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const sellTransaction = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_sellTransaction_data' + JSON.stringify(data));
    let response = await axios.post(config().offRamp, data);

    console.log('Action_sellTransaction_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_sellTransaction_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};

export const CreateRzpOrder = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_CreateRzpOrder_data' + JSON.stringify(data));
    let response = await axios.post(config().rzpOrder, data);

    console.log('Action_CreateRzpOrder_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_CreateRzpOrder_error' + JSON.stringify(e));
    alert(e)
    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};

export const PayRzpOrder = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_ PayRzpOrder _data' + JSON.stringify(data));
    let response = await axios.put(config().pay + data.paymentId, data);

    console.log('Action_PayRzpOrder _response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_PayRzpOrder _error' + JSON.stringify(e));
    alert(e)
    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};


export const checkKYc = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_checkKYc _data' + JSON.stringify(data));
    let response = await axios.post(config().getApplicant, data);

    console.log('Action_checkKYc _response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_checkKYc _error' + JSON.stringify(e));
    // alert(e)
    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
