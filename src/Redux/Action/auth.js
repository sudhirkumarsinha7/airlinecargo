/* eslint-disable sort-keys */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { USERINFO, LOGIN, REGISTER, SELECTEDLANG } from '../constant';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { config, getIosDeviceTokeApi, auth } from '../../config/config';

import setAuthToken from '../../config/setAuthToken';
import { loadingOff, loadingOn } from './loader';
export const changeLang = (lng, token = '') => async dispatch => {
  console.log('changeLang ' + lng)
  if (token) {
    setAuthToken(token, lng);
  }
  dispatch({
    type: SELECTEDLANG,
    payload: lng,
  });
};
export const Register = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('Action_sRegister_data' + JSON.stringify(data));
    let response = await axios.post(config().register, data);

    console.log('Action_sRegister_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_Register_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};
export const LogIn = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_sLogIn_data' + JSON.stringify(data));
    let response = await axios.post(config().login, data);

    console.log('Action_LogIn_response' + JSON.stringify(response.data.data));
    if (response?.data?.data?.token) {
      await AsyncStorage.setItem('token', response?.data?.data?.token);

    }

    dispatch({
      type: USERINFO,
      payload: response.data.data,
    });
    setAuthToken(response.data && response.data.data && response?.data?.data?.token)
    return response;
  } catch (e) {
    console.log('Action_LogIn_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }

};