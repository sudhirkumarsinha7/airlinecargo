import React, { useRef, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Alert,
    TouchableOpacity,
    StatusBar,
    ScrollView,
    ImageBackground
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { CustomTextView } from '../../Components/Common/heper';
import { CustomButton, CustomButtonLine } from '../../Components/Common/Button';
import { scale } from '../../Components/Scale';
import fonts from '../../constants/fonts';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Colors } from '../../Components/Common/Style';
import { CustomInputText, SubHeaderComponent, CustomTextLine } from "../../Components/Common/heper"
import { Header } from '../../Components/Header'
import { DeviceWidth, localImage } from '../../config/global';

const Home = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const insets = useSafeAreaInsets();

    return (
        // <ImageBackground
        //     source={localImage.backgraound}
        //     resizeMode="cover"
        //     style={styles.imgBackground}
        // >
        <View style={{ flex: 1 }}>

            <StatusBar
                hidden={false}
                backgroundColor={'#fff'}
                barStyle={'dark-content'}
            />
            <Header name={'Home'} navigation={props.navigation} onPressLeft={() => console.log('')} textColor={'#fff'} />
            <ScrollView style={{ marginHorizontal: 15, padding: 15, borderRadius: 10, backgroundColor: '#fff' }}>

                <Text style={styles.title}>{'Flight Details'}</Text>

                <CustomTextView
                    leftText={'Airline'}
                    rightText={'Indigo'}
                />
                <CustomTextView
                    leftText={'Flight'}
                    rightText={'9E-8976'}
                />
                <CustomTextView
                    leftText={'Flight Date'}
                    rightText={'06/12/2023'}
                />
                <CustomTextView
                    leftText={'Flight Time'}
                    rightText={'8:00 PM'}
                />
                <Text style={styles.title}>{'Cargo Details'}</Text>


                <CustomTextView
                    leftText={'MAWB'}
                    rightText={'987-78678585'}
                />
                <CustomTextView
                    leftText={'Gr Weight'}
                    rightText={'10 Kg'}
                />
                <CustomTextView
                    leftText={'Ch. Weight'}
                    rightText={'5 kg'}
                />

                <CustomTextView
                    leftText={'Exp pcs'}
                    rightText={'10'}
                />
                <CustomTextView
                    leftText={'SHC1,SHC2,SHC3'}
                    rightText={'CAA,IND,AUS'}
                />
                <CustomTextView
                    leftText={'Country/AOD'}
                    rightText={'Inida/Hyd'}
                />

                <CustomTextView
                    leftText={'Commodity Type'}
                    rightText={'Pharma'}
                />
                <CustomTextView
                    leftText={'Commodity'}
                    rightText={'Pharma1'}
                />
                <Text style={styles.title}>{'Truck Details'}</Text>


                <CustomTextView
                    leftText={'Driver Name'}
                    rightText={'Rajshekhar'}
                />

                <CustomTextView
                    leftText={'Driver ID'}
                    rightText={'123456677'}
                />
                <CustomTextView
                    leftText={'Truck No'}
                    rightText={'TN12455'}
                />
                <CustomButton
                    bgColor={'#FF6A28'}
                    buttonName={t('Make Payment')}
                    navigation={props.navigation}
                    onPressButton={() => props.navigation.navigate('shipment')}
                />
            </ScrollView>
        </View>
        // </ImageBackground>
    );
};

const styles = StyleSheet.create({
    title: { fontSize: scale(16), fontFamily: fonts.BOLD, color: '#FF6A28', fontWeight: '500', marginLeft: 10, marginTop: 15 },

    subTitle: {
        fontSize: scale(13),
        fontFamily: fonts.REGULAR,
        color: '#888889',
        marginVertical: 10,
    },
    phoneInputContainer: {
        width: '100%',
        overflow: 'visible',
        borderWidth: 1,
        borderRadius: 1,
        overflow: 'visible',
        borderColor: '#a8a8a8',
        // marginBottom: 5,
    },
    phoneInputTextContainer: {
        // overflow: 'visible',
        backgroundColor: '#FFFFFF',
    },
    phoneInputText: { padding: 0 },
});
export default Home;
