import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native'

import { Colors } from './Style';
import fonts from '../../constants/fonts';
import { scale } from '../Scale';
import { useTranslation } from 'react-i18next';


export default function CustomRadioButton({ isSelected, onPress, title, width, needBgColor, txtColor, trans }) {
    const {
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT
    } = Dimensions.get('window');
    const { t, i18n } = useTranslation();

    return (
        <TouchableOpacity
            style={[{
                backgroundColor: isSelected && needBgColor ? "#F0F1F4" : trans ? 'rgba(77, 42, 160, 0.02)' : "#fff",
                width: width ? width : SCREEN_WIDTH * 0.9
            }, styles.radio_btn_container]}
            onPress={onPress}>
            <View style={[{ borderColor: isSelected ? Colors.primary : "#C8D0D7" }, styles.radio_btn_border]}>
                <View style={[{ backgroundColor: isSelected ? Colors.primary : "transparent" }, styles.radio_btn_inside]} />
            </View>

            <Text
                style={[{ color: txtColor != "" && txtColor == undefined ? 'gray' : txtColor.toString() }, styles.radio_btn_text]} >
                {t(title)}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    radio_btn_container: {
        height: 40,
        alignSelf: "center",
        borderRadius: 15,
        // marginTop: 10,
        flexDirection: "row",
        paddingLeft: 5,
        alignItems: "center",
    },
    radio_btn_border: {
        width: 22,
        height: 22,
        borderRadius: 12,
        borderWidth: 2,
        borderColor: '#dcdcdc',
        alignItems: 'center',
        justifyContent: 'center'
    },
    radio_btn_inside: {
        width: 12,
        height: 12,
        borderRadius: 6,
    },
    radio_btn_text: {
        fontSize: scale(13.5),
        marginLeft: 10,
        fontFamily: fonts.REGULAR,
        alignSelf: "center",
    }
})