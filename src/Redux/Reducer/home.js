/* eslint-disable sort-keys */
import {
  ETH_TO_USD,
  SWAPCRYPTO,
  BUYCRYPTO,
  SELLCRYPTO,
  CURRENCY_LIST
} from '../constant';

const initialState = {
  swap_list: [],
  buy_list: [],
  sell_list: [],
  eth_usd: {},
  currency_list:[],
};
export default function (state = initialState, action) {
  switch (action.type) {
    case SWAPCRYPTO:
      return {
        ...state,
        swap_list: action.payload,
      };
    case BUYCRYPTO:
      return {
        ...state,
        buy_list: action.payload,
      };
    case ETH_TO_USD:
      return {
        ...state,
        eth_usd: action.payload,
      };
    case SELLCRYPTO:
      return {
        ...state,
        sell_list: action.payload,
      };
      case CURRENCY_LIST:
      return {
        ...state,
        currency_list: action.payload,
      };
   default:
      return state;
  }
}
