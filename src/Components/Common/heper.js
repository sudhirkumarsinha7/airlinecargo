
import React, { useEffect, useState, useRef } from 'react';
import {
    Image,
    View,
    Text,
    TouchableOpacity,
    Dimensions,
    TextInput,
    StyleSheet, Alert,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../Scale';
import { CommonStyle, Colors, DeviceWidth } from './Style';
import DatePicker from 'react-native-date-picker'
import _ from 'lodash';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign'
import fonts from '../../constants/fonts';

import { localImage } from '../../config/global';

export const RenderLabel = (props) => {
    return (
        <Text style={[styles.label, { color: props.color }]}>
            {props.title}
        </Text>
    );
};
export const CustomButton = props => {
    const { buttonName = '', onPressButton, image = '', bgColor = '' } = props
    return (<TouchableOpacity onPress={() => onPressButton()} style={{ justifyContent: 'center', alignItems: 'center', margin: scale(10), backgroundColor: bgColor ? bgColor : Colors.blueF4, padding: scale(7), borderRadius: scale(10), flexDirection: 'row' }}>
        {image ? <Image
            style={{ width: scale(14), height: scale(14), marginRight: scale(5) }}
            source={image}
            resizeMode="contain"
        /> : null}
        <Text style={{ color: Colors.whiteFF, fontWeight: '400', fontSize: scale(14) }}>{buttonName}</Text>
    </TouchableOpacity>
    )
}
export const CustomButtonWithBorder = props => {
    const { buttonName = '', onPressButton, bgColor = '', image = '' } = props
    return (<TouchableOpacity onPress={() => onPressButton()} style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', margin: scale(10), borderColor: bgColor ? bgColor : Colors.blueF4, borderWidth: 1, padding: scale(7), borderRadius: scale(10) }}>
        {image ? <Image
            style={{ width: scale(14), height: scale(14), marginRight: scale(5) }}
            source={image}
            resizeMode="contain"
        /> : null}
        <Text style={{ color: bgColor ? bgColor : Colors.blueF4, fontWeight: '500', fontSize: scale(14) }}>{buttonName}</Text>
    </TouchableOpacity>
    )
}
export const CustomTextLine = props => {
    const { name = '', textColor = '' } = props
    return (<View style={{ marginTop: scale(7) }}>

        <Text style={{ color: textColor ? textColor : Colors.black0, fontWeight: '400', fontSize: scale(13) }}>{name}</Text>
    </View>
    )
}
export const CustomButtonLine = props => {
    const { buttonName = '', onPressButton, image = '' } = props
    return (<TouchableOpacity onPress={() => onPressButton()} style={{ marginTop: scale(10), flexDirection: 'row' }}>
        {image ? <Image
            style={{ width: scale(14), height: scale(14), marginRight: scale(5) }}
            source={image}
            resizeMode="contain"
        /> : null}
        <Text style={{ color: Colors.blueFF, fontWeight: '400', fontSize: scale(12) }}>{buttonName}</Text>
    </TouchableOpacity>
    )
}
export const DataSafeView = props => {
    return (<View style={{ marginTop: scale(10), flexDirection: 'row' }}>
        <Image
            style={{ width: scale(14), height: scale(14), marginRight: scale(5) }}
            source={localImage.lock}
            resizeMode="contain"
        />
        <Text style={{ color: Colors.garyA6, fontWeight: '400', fontSize: scale(12) }}>{'Your Info is safely secured'}</Text>
    </View>
    )
}
export const StatusComponent = props => {
    return (
        <View style={{ flexDirection: 'row', }}>
            <View style={{ backgroundColor: '#D9F0D6', flexDirection: 'row', padding: scale(5), borderRadius: scale(5), paddingLeft: scale(15), paddingRight: scale(15) }}>

                <MaterialIcons name={"check-circle-outline"} size={20} color={'#56BD4D'} />

                <Text style={{ fontWeight: '400', fontSize: scale(12), marginLeft: scale(5) }}>{props.name}</Text>
            </View>
        </View>
    )
}
export const ErrorComponent = props => {
    return <View>
        <Text
            style={{
                color: 'red',
                marginTop: scale(5),
                fontSize: scale(10)
            }}>
            {props.errorMsg}
        </Text>
    </View>
}
export const SubHeaderComponent = props => {
    const { textColor = Colors.black0 } = props
    return (
        <View style={{ margin: scale(10) }}>
            <Text style={{ fontSize: scale(16), color: textColor, fontWeight: '500', }}>
                {props.name}
            </Text>
        </View>
    )
}


export const CustomTextViewVertical = (props) => {
    return (
        <View style={{ backgroundColor: Colors.whiteFF }}>
            <View style={{ padding: 10, borderRadius: 10 }}>
                <Text style={{ fontSize: scale(16), color: Colors.rightBlack, fontWeight: '700', marginBottom: 3 }}>{props.topText}</Text>
                <Text style={{ fontSize: scale(12), color: Colors.rightBlack, fontWeight: '400' }}>{props.bottomText}</Text>
            </View>
        </View>
    )
}
export const CustomInputText = (props) => {
    const [isFocus, setIsFocus] = useState(false);
    const inputRef = useRef();

    const {
        label = '',
        mandatory = false,
        onChange,
        val,
        errorMsg = '',
        disabled = false,
        keyboardType = '',
        maxlenth = 100,
        placeholderText = '',
        islabel = true
    } = props;

    return (


        <View style={styles.container}>
            {islabel && val ? <RenderLabel title={label} color={disabled ? '#646867' : Colors.black0} /> : null}
            <TextInput
                placeholder={placeholderText}
                value={val}
                style={[styles.dropdown, isFocus && { borderColor: Colors.black0 }]}
                onChangeText={onChange}
                editable={!disabled}
                onFocus={() => setIsFocus(true)}
                maxLength={maxlenth}
                keyboardType={keyboardType}
                ref={inputRef}
            />
            {mandatory ? <Text
                style={{
                    color: 'red',
                    marginTop: scale(5),
                    fontSize: scale(10)
                }}>
                {errorMsg}
            </Text> : null}
        </View>
    );
}

export const CustomInputPassword = (props) => {
    const [showpass, setShowpass] = useState(false);
    const [isFocus, setIsFocus] = useState(false);

    const {
        label = '',
        mandatory = false,
        onChange,
        val,
        errorMsg = '',
        disabled = false,
        keyboardType = ''
    } = props;

    return (


        <View style={{ ...styles.container, }}>
            <RenderLabel title={label} color={disabled ? '#646867' : Colors.black0} />

            <View style={{
                ...styles.dropdown,
                flexDirection: 'row',
            }}>
                <TextInput
                    placeholder={label}
                    label={label}
                    value={val}
                    onChangeText={onChange}
                    secureTextEntry={!showpass}
                    style={{ flex: 1 }}
                    onFocus={() => setIsFocus(true)}

                />
                <TouchableOpacity style={{ justifyContent: 'center' }} onPress={() => setShowpass(!showpass)}>
                    <MaterialCommunityIcons name={showpass ? "eye" : "eye-off"} size={25} color={Colors.black0} />
                </TouchableOpacity>
            </View>
            {mandatory ? <Text
                style={{
                    color: 'red',
                    fontSize: scale(10),
                    marginTop: scale(5),

                }}>
                {errorMsg}
            </Text> : null}
        </View>
    );
}
export const CustomInputRightIcon = (props) => {
    const [isFocus, setIsFocus] = useState(false);

    const {
        label = '',
        mandatory = false,
        onChange,
        val,
        errorMsg = '',
        disabled = false,
        keyboardType = ''
    } = props;

    return (


        <View style={{ ...styles.container, }}>
            <RenderLabel title={label} color={disabled ? '#646867' : Colors.black0} />

            <View style={{
                ...styles.dropdown,
                flexDirection: 'row',
            }}>
                <TextInput
                    placeholder={label}
                    label={label}
                    value={val}
                    onChangeText={onChange}
                    style={{ flex: 1 }}
                    onFocus={() => setIsFocus(true)}

                />

                <View style={{ justifyContent: 'center', borderLeftWidth: 0.5, paddingLeft: scale(5), borderColor: disabled ? '#646867' : Colors.black0 }} >
                    <Text style={{ fontSize: scale(14), color: disabled ? '#646867' : Colors.black0, textAlign: 'center' }}>KG</Text>
                </View>
            </View>
            {mandatory ? <Text
                style={{
                    color: 'red',
                    fontSize: scale(10),
                    marginTop: scale(5),

                }}>
                {errorMsg}
            </Text> : null}
        </View>
    );
}



























export const CustomTextView = (props) => {
    const { leftTextColor = Colors.black0, rightTextColor = Colors.leftgray, leftfontWeight = fonts.MEDIUM, rightfontWeight = fonts.MEDIUM, leftText = '', rightText = '' } = props
    return (
        <View style={{ flexDirection: 'row', margin: 5, marginLeft: 10 }}>
            <View style={{ flex: 0.5 }}>
                <Text style={{ fontSize: scale(14), color: leftTextColor, fontWeight: leftfontWeight }}>{leftText}</Text>
            </View>
            <View style={{ flex: 0.5 }}>
                <Text style={{ fontSize: scale(14), color: rightTextColor, fontWeight: rightfontWeight }}>{rightText}</Text>
            </View>
        </View>
    )
}

export const CustomTextView1 = (props) => {
    return (
        <View style={{ flexDirection: 'row', }}>
            <View style={{ flex: 0.4 }}>
                <Text style={{ fontSize: scale(8), color: Colors.leftgray, fontWeight: props.leftfontWeight || 'normal' }}>{props.leftText}</Text>
            </View>
            <View style={{ flex: 0.6, marginLeft: 5 }}>
                <Text style={{ fontSize: scale(8), color: Colors.rightBlack, fontWeight: props.rightfontWeight || 'normal' }}>{props.rightText}</Text>
            </View>
        </View>
    )
}

export const CustomTextView2 = (props) => {
    return (
        <View style={{ flexDirection: 'row', margin: 5, marginLeft: 10 }}>
            <View style={{ flex: 0.5 }}>
                <Text style={{ fontSize: scale(13), color: Colors.black70, fontWeight: props.leftfontWeight || 'normal' }}>{props.leftText}</Text>
            </View>
            <View style={{ flex: 0.5 }}>
                <Text style={{ fontSize: scale(13), color: Colors.rightBlack, fontWeight: props.rightfontWeight || 'normal' }}>{props.rightText}</Text>
            </View>
        </View>
    )
}



export const CustomTextViewVertical1 = (props) => {
    return (
        <View style={{ margin: 5, marginLeft: 10 }}>
            <Text style={{ fontSize: scale(14), color: Colors.leftgray, fontWeight: props.leftfontWeight || 'normal' }}>{props.leftText}</Text>

            <Text style={{ fontSize: scale(14), color: Colors.rightBlack, fontWeight: props.rightfontWeight || 'normal' }}>{props.rightText}</Text>
        </View>
    )
}
export const CustomTextViewVerticalNew = (props) => {
    const { isDate = false } = props
    return (
        <View style={{ margin: 5, marginLeft: 10 }}>
            <Text style={{ fontSize: scale(10), color: '#716D74', fontWeight: '400', }}>{props.topText}</Text>
            <Text style={{ fontSize: isDate ? scale(8) : scale(12), fontWeight: '400', marginTop: scale(5) }}>{props.bottomText}</Text>
        </View>
    )
}



export const CustomeDatePicker = (props) => {
    const {
        eachRow,
        state,
        updateState,
        edit = true,
        minDate = '',
        image = '',
    } = props;
    return (
        <View
            style={{
                marginBottom: 8,
            }}>
            {/* <View>
                {state[eachRow.stateName] ? <View>
                    <Text
                        style={{
                            color: '#707070',
                        }}>
                        {eachRow.displayName}
                    </Text>
                </View> : null}
            </View> */}
            <View
                style={{
                    width: '100%',
                    height: scale(35),
                    borderBottomColor: '#E8E8E8',
                    borderBottomWidth: 1,
                }}>
                <DatePicker
                    style={{
                        height: scale(35),
                        width: '95%',
                    }}
                    date={state[eachRow.stateName]} //initial date from state
                    mode="date" //The enum of date, datetime and time
                    placeholder="DD/MM/YYYY"
                    placeHolderTextStyle={{
                        color: '#707070',
                        fontSize: scale(15),
                    }}
                    format="DD/MM/YYYY"
                    // minDate={minDate ? minDate : new Date()}
                    maxDate="01-01-2100"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateInput: {
                            borderWidth: 0,
                            fontSize: scale(15),
                            height: scale(50),
                            width: '100%',
                            alignItems: 'flex-start',
                        },
                    }}
                    onDateChange={date => {
                        updateState({ [eachRow.stateName]: date });
                    }}
                />
            </View>
        </View>
    );
}
export const CustomeDatePickerNew = (props) => {
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const [isFocus, setIsFocus] = useState(false);


    const {
        label = '',
        mandatory = false,
        onChange,
        val,
        minDate = '',
        mandatoryStrik = true,
        disabled = false
    } = props;


    // console.log('CustomInputText errorMsg '+errorMsg)

    const onValueChange = (newDate) => {


        // console.log('newdate  ', newDate)
        const CurrentYear = new Date(newDate).getFullYear();
        const CurrentMonth = new Date(newDate).getMonth()
        const CurrentDate = new Date(newDate).getDate()
        let newFormatDate = new Date(CurrentYear, CurrentMonth, CurrentDate)
        setShow(false)

        onChange(newFormatDate)

    }

    return (
        <View style={{ ...styles.container, }}>
            <RenderLabel title={label} color={disabled ? '#646867' : Colors.black0} />

            <TouchableOpacity style={{
                ...styles.dropdown,
                flexDirection: 'row',
            }} onPress={() => setShow(true)} >

                <TextInput
                    placeholder={label}
                    label={label}
                    value={val ? val : "DD/MM/YYYY"}
                    onChangeText={() => setShow(true)}
                    style={{ flex: 1 }}
                    onFocus={() => setIsFocus(true)}

                />
                <View style={{ justifyContent: 'center' }}>
                    {/* <Image
                        style={{ width: scale(14.04), height: scale(14.04) }}
                        source={require('../../assets/Calander-menu-active.png')}
                        resizeMode="contain"
                    /> */}
                </View>

            </TouchableOpacity>




            {mandatory && !val ? <Text
                style={{
                    color: Colors.red,
                    margin: 5,
                    marginLeft: 12,
                    fontSize: 12,
                }}>
                {'Required'}
            </Text> : null}
            <DatePicker
                modal
                open={show}
                date={new Date()}
                mode="date"
                minimumDate={new Date()}
                maximumDate={new Date("2051-12-31")}
                onConfirm={(date) => {
                    onValueChange(date)
                }}
                onCancel={() => {
                    setShow(false)
                }}
            />
        </View>
    );
}
// export const CustomInputText1 = (props) => {
//     const {
//         label = '',
//         mandatory = false,
//         onChange,
//         val,
//         errorMsg = '',
//         disabled = false,
//         keyboardType = ''
//     } = props;
//     // console.log('CustomInputText errorMsg '+errorMsg)
//     return (

//         // <Input
//         //     placeholder={label}
//         //     label={val || disabled ? label : ''}
//         //     labelStyle={CommonStyle.labelStyleOrange}
//         //     placeholderTextColor={Colors.grayB1}
//         //     onChangeText={onChange}
//         //     value={val}
//         //     errorMessage={mandatory ? errorMsg : null}
//         //     disabled={disabled}
//         //     keyboardType={keyboardType}
//         //     style={{ color: Colors.black0 }}
//         // />
//         <View style={{ marginLeft: scale(15), marginRight: scale(15), marginTop: scale(10) }}>
//             <TextInput
//                 mode="outlined"
//                 // placeholder={'outlined'}
//                 label={label}
//                 value={val}
//                 style={{ borderRadius: 8, }}
//                 onChangeText={onChange}
//                 disabled={disabled}
//                 theme={{ colors: { primary: Colors.black0, underlineColor: Colors.black0, backgroundColor: 'white' }, }}
//             />
//             {mandatory ? <Text
//                 style={{
//                     color: 'red',
//                     marginTop: scale(5),
//                     marginBottom: scale(5),
//                     fontSize: scale(10)
//                 }}>
//                 {errorMsg}
//             </Text> : null}
//         </View>
//     );
// }
export const CustomAwbSearch = (props) => {
    const [isFocus, setIsFocus] = useState(false);
    const inputRef = useRef();

    const {
        label = '',
        label1 = '',
        mandatory = false,
        onChange,
        onChange1,
        val,
        val1,
        errorMsg = '',
        disabled = false,
        keyboardType = '',
        maxlenth = 100,
        errorMsg1 = '',
    } = props;

    return <View style={{ flexDirection: 'row', }}>
        <View style={{ flex: 0.4 }}>

            <View style={styles.container}>
                <RenderLabel title={label} color={disabled ? '#646867' : Colors.black0} />

                <TextInput
                    placeholder={label}
                    label={label}
                    value={val}
                    style={[styles.dropdown, isFocus && { borderColor: Colors.black0 }]}

                    onChangeText={(value) => {
                        onChange(value)
                        if (value.length === 3) {
                            inputRef.current.focus();
                        }
                    }}
                    disabled={disabled}
                    onFocus={() => setIsFocus(true)}
                    maxLength={3}
                    keyboardType={keyboardType}
                    returnKeyType="next"
                />
                <Text
                    style={{
                        color: 'red',
                        marginTop: scale(5),
                        fontSize: scale(10)
                    }}>
                    {errorMsg}
                </Text>
            </View>

        </View>
        <View style={{ flex: 0.5 }}>

            <View style={styles.container}>
                <RenderLabel title={label1} color={disabled ? '#646867' : Colors.black0} />

                <TextInput
                    placeholder={label1}
                    label={label1}
                    value={val1}
                    style={[styles.dropdown, isFocus && { borderColor: Colors.black0 }]}
                    onChangeText={onChange1}
                    disabled={disabled}
                    onFocus={() => setIsFocus(true)}
                    maxLength={8}
                    keyboardType={keyboardType}
                    ref={inputRef}
                    returnKeyType="next"
                />
                <Text
                    style={{
                        color: 'red',
                        marginTop: scale(5),
                        fontSize: scale(10)
                    }}>
                    {errorMsg1}
                </Text>
            </View>

        </View>
    </View>
}




const styles = StyleSheet.create({
    content: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        flexShrink: 1,
    },
    inputSearchStyle: {
        fontSize: scale(14),
    },



    selectedTextStyle1: {
        color: Colors.blueFF,
        fontSize: scale(14),
    },
    borderStyle: {},
    borderStyle1: {},


    textInputStyle: {
        padding: Platform.OS === 'ios' ? 10 : 7,
        color: '#333333',
        fontSize: scale(16)
    },
    textInputStyleDisabled: {
        padding: Platform.OS === 'ios' ? 10 : 7,
        color: 'gray',
        fontSize: scale(16)
    },
    labelStyle: { fontWeight: '600', marginLeft: scale(5), fontSize: scale(12), marginBottom: scale(5), color: Colors.black0 },
    textViewStyle: {
        marginTop: 10,
        margin: 10
    },
    textInputViewStyle: {
        marginTop: 5,
        borderWidth: 1,
        borderColor: '#D9D9D9',
        padding: 2,
        borderRadius: 5,
    },
    bottomBorder: {
        borderBottomWidth: 1,
        borderColor: '#D9D9D9',
    },
    placeHolderText: {
        color: '#BCBCBD',
        fontWeight: 'normal',
        fontSize: scale(14)
    },
    container: {
        backgroundColor: 'white',
        paddingTop: scale(16),
        // padding: scale(5)
    },
    dropdown: {
        height: scale(35),
        borderColor: '#BCBCBD',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
    },
    icon: {
        marginRight: 5,
    },
    label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: scale(14)
    },
    placeholderStyle: {
        color: 'gray',
        fontSize: scale(14)

    },
    selectedTextStyle: {
        fontSize: scale(14),
        // color: Colors.blueFF
    },
    selectedTextMultiStyle: {
        fontSize: scale(14),
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: scale(40),
        fontSize: scale(14)
    },
    item: {
        padding: 17,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    selectedStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 14,
        backgroundColor: '#F3F5F7',
        shadowColor: '#000',
        marginTop: 8,
        marginRight: 12,
        paddingHorizontal: 12,
        paddingVertical: 8,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,

        elevation: 2,
        borderColor: Colors.blueF4,
        borderWidth: 1,
    },
    textSelectedStyle: {
        marginRight: 5,
        fontSize: scale(14),
        color: Colors.blueF4
    },


});