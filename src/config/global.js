/* eslint-disable no-sparse-arrays */
import { Dimensions } from 'react-native';
export const DeviceHeight = Dimensions.get('window').height;
export const DeviceWidth = Dimensions.get('window').width;

export const localImage = {

  backgraound: require('../assets/backgraound.png'),
  STATWIG: require('../assets/STATWIG.png'),
  logo: require('../assets/CARGOLEDGER.png'),


};
export const userInfo = {
  name: 'Xyz ',
  email: 'abc@gmail.com',
  comment: 'Why Netflix & Chill when you can NFT & Chill? 😎',
};
export const langList = [
  { name: 'English', id: 'en', icon: 'A' },
  { name: 'Hindi', id: 'hi', icon: 'ह' },
  { name: 'Telugu', id: 'tl', icon: 'అ' },

];
export const defaultlang = 'hi'
export const Colors = {
  red00: 'red ',
  blue: 'blue',
  black: 'black',
};
