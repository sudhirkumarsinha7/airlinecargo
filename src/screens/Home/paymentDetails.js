import React, { useRef, useState } from 'react';

import { View, Text, TouchableOpacity, TextInput, StyleSheet, Image, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useTranslation } from 'react-i18next';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { CustomTextView } from '../../Components/Common/heper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { scale } from '../../Components/Scale';
import fonts from '../../constants/fonts';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Colors } from '../../Components/Common/Style';
import { CustomInputText, SubHeaderComponent, CustomTextLine } from "../../Components/Common/heper"
import { Header1 } from '../../Components/Header'
import { CustomButton, CustomButtonLine } from '../../Components/Common/Button';

const Home = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const insets = useSafeAreaInsets();
    const [cardNumber, setCardNumber] = useState('');
    const [expiryDate, setExpiryDate] = useState('');
    const [cvv, setCVV] = useState('');

    const handleCardPayment = () => {
        // Handle card payment logic
        props.navigation.navigate('success')
    };

    const handleUpiPayment = () => {
        // Handle UPI payment logic
        props.navigation.navigate('success')
    };
    return (
        <View
            style={{
                flex: 1,

            }}>
            <StatusBar
                hidden={false}
                backgroundColor={'#fff'}
                barStyle={'dark-content'}
            />
            <Header1 isBack={true} name={'Payment Details'} navigation={props.navigation} textColor={'#fff'} />

            <View style={{ backgroundColor: Colors.whiteFF, margin: 15, padding: 15, borderRadius: 10 }}>
                <View style={styles.paymentDetails}>
                    <Text style={styles.paymentAmount}>₹ 1,600</Text>
                    <Text style={styles.paymentDescription}>Payment </Text>
                </View>

                <View style={styles.cardDetails}>
                    <TextInput
                        style={styles.input}
                        placeholder="Card Number"
                        keyboardType="numeric"
                        value={cardNumber}
                        onChangeText={(text) => setCardNumber(text)}
                    />
                    <View style={styles.cardRow}>
                        <TextInput
                            style={[styles.input, { flex: 1, marginRight: 10 }]}
                            placeholder="Expiry Date"
                            keyboardType="numeric"
                            value={expiryDate}
                            onChangeText={(text) => setExpiryDate(text)}
                        />
                        <TextInput
                            style={[styles.input, { flex: 1 }]}
                            placeholder="CVV"
                            keyboardType="numeric"
                            value={cvv}
                            onChangeText={(text) => setCVV(text)}
                        />
                    </View>
                </View>

                <TouchableOpacity style={styles.paymentButton} onPress={handleCardPayment}>
                    <Text style={styles.paymentButtonText}>Pay with Card</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.upiButton} onPress={handleUpiPayment}>
                    <Icon name="payment" size={24} color="white" />
                    <Text style={styles.upiButtonText}>Pay with UPI</Text>
                </TouchableOpacity>

            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F3F3F3', // Light background color
    },
    logo: {
        width: 100,
        height: 100,
        marginBottom: 20,
    },
    paymentDetails: {
        alignItems: 'center',
        marginBottom: 20,
    },
    paymentAmount: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#333', // Dark text color
    },
    paymentDescription: {
        fontSize: 16,
        color: '#666', // Medium text color
    },
    cardDetails: {
        width: '80%',
        marginBottom: 20,
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        paddingLeft: 10,
    },
    cardRow: {
        flexDirection: 'row',
    },
    paymentButton: {
        backgroundColor: '#FF4081', // Pink color
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 10,
        marginBottom: 10,
    },
    paymentButtonText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    },
    upiButton: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#536DFE', // Indigo color
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 10,
    },
    upiButtonText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 10,
    },
});
export default Home;
