import React, { useEffect, useState, useRef } from 'react';
import {
  Button,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';

import { Colors, DeviceWidth, CommonStyle } from './Style';
import { moderateScale, scale, verticalScale } from '../Scale';
import LinearGradient from 'react-native-linear-gradient';
import { join } from 'path';
import fonts from '../../constants/fonts';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { useTranslation } from 'react-i18next';

export const CustomButton = props => {
  const {
    buttonName = '',
    onPressButton,
    image = '',
    bgColor = '',
    disabled = false,
  } = props;
  return (
    <TouchableOpacity
      onPress={() => onPressButton()}
      disabled={disabled}
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        margin: scale(10),
        height: 50,
        // paddingVertical: Platform.OS == "ios" ? scale(10) : 0,
        backgroundColor: bgColor ? bgColor : Colors.blueF4,
        // padding: scale(7),
        borderRadius: scale(20),
        flexDirection: 'row',
      }}>
      {image ? (
        <Image
          style={{ width: scale(14), height: scale(14), marginRight: scale(5) }}
          source={image}
          resizeMode="contain"
        />
      ) : null}
      <Text
        style={{ color: Colors.whiteFF, fontWeight: '700', fontSize: scale(16), fontFamily: fonts.BOLD }}>
        {buttonName}
      </Text>
    </TouchableOpacity>
  );
};
export const CustomButtonWithBorder = props => {
  const { buttonName = '', onPressButton, image = '', bdColor = Colors.grayB1 } = props;
  return (
    <TouchableOpacity
      onPress={() => onPressButton()}
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        margin: scale(10),
        height: 50,
        // paddingVertical: Platform.OS == "ios" ? scale(10) : 0,
        borderColor: bdColor,
        borderWidth: 1,
        // padding: scale(7),
        borderRadius: scale(20),
        flexDirection: 'row',

      }}>
      {image ? (
        <Image
          style={{ width: 30, height: 30, margin: scale(5) }}
          source={image}
          resizeMode="contain"
        />
      ) : null}
      <Text
        style={{
          color: Colors.black0,
          fontFamily: fonts.MEDIUM,
          fontSize: scale(13.5),
        }}>
        {buttonName}
      </Text>
    </TouchableOpacity>
  );
};

export const CustomButtonLine = props => {
  const { buttonName = '', onPressButton, image = '', bgColor = Colors.blueFF } = props
  return (<TouchableOpacity onPress={() => onPressButton()} style={{ flexDirection: 'row', marginTop: scale(7) }}>
    {image ? <Image
      style={{ width: scale(14), height: scale(14), marginRight: scale(5) }}
      source={image}
      resizeMode="contain"
    /> : null}
    <Text style={{ color: bgColor, fontWeight: '400', fontSize: scale(12) }}>{buttonName}</Text>
  </TouchableOpacity>
  )
}


export const CardNavigationBtn = props => {
  const { title, image, type, onPressButton } = props;
  const { t, i18n } = useTranslation();

  return (
    <TouchableOpacity onPress={() => onPressButton()}
      style={{ height: 45, marginVertical: 5, flexDirection: 'row', paddingHorizontal: 10 }}>
      <View style={{ width: '90%', flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: '#ede6f7', borderRadius: 100 }}>
          {
            type == "icon" ? image :
              <Image source={image} style={{ height: 25, width: 25 }} />
          }
        </View>
        <Text style={{ fontFamily: fonts.REGULAR, color: '#000', fontSize: scale(13), paddingLeft: 10 }}>{t(title)}</Text>
      </View>
      <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center', }}>
        <Ionicons name={"chevron-forward"} size={20} color={'#888889'} />
      </View>
    </TouchableOpacity>
  )
}

export const CardBtn = props => {
  const { visible, title, onPress } = props;
  return (
    <View style={{ flexDirection: 'row', height: 50, justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
      <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', paddingVertical: 10, fontSize: scale(14) }}>{title}</Text>
      <TouchableOpacity onPress={onPress} style={{ height: '90%', width: 40, alignItems: 'center', justifyContent: 'center' }}>
        <Ionicons name={visible ? "chevron-down" : "chevron-forward"} size={25} color={'#000'} />
      </TouchableOpacity>
    </View>
  )
}
