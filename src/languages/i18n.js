import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from './en.json';
import hi from './hi.json';
import te from './te.json';

// i18n.use(initReactI18next).init({
//     lng: 'hi',
//     fallbackLng: 'hi',
//     compatibilityJSON: 'v3',
//     resources: {
//         en: en,
//         es: hi,
//         te: te
//     },
//     interpolation: {
//         escapeValue: false // react already safes from xss
//     }
// });

// export default i18n;
i18n.use(initReactI18next).init({
    resources: {
        en: en,
        hi: hi,
        te: te
    },
    lng: 'hi',
    fallbackLng: 'hi',
    compatibilityJSON: 'v3',

    interpolation: {
        escapeValue: false,
    },
});

export default i18n;