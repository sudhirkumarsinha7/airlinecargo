import React, { useState } from "react";
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import { useTranslation } from 'react-i18next';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { useSafeAreaInsets } from "react-native-safe-area-context";

import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import fonts from "../../constants/fonts";
import { Colors } from "../../Components/Common/Style";
import { scale } from "../../Components/Scale";
import { ConditionalBtn, CardBtn, CardNavigationBtn } from "../../Components/Common/Button";


const Settings = (props) => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const insets = useSafeAreaInsets()

    return (
        <ScrollView style={{ backgroundColor: '#fff', paddingHorizontal: 10 }}
            contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ borderBottomWidth: 1, marginBottom: 10, borderColor: '#dcdcdc' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', paddingVertical: 10, fontSize: scale(14) }}>{t('Navigation')}</Text>
                <TouchableOpacity onPress={() => { }}
                    style={{ height: 45, marginVertical: 5, flexDirection: 'row', paddingHorizontal: 10 }}>
                    <View style={{ width: '50%', flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: '#ede6f7', borderRadius: 100 }}>
                            <Ionicons name="map-outline" size={25} color={Colors.primary} />
                        </View>
                        <Text style={{ fontFamily: fonts.REGULAR, color: '#000', fontSize: scale(13), paddingLeft: 10 }}>{t('Map')}</Text>
                    </View>
                    <TouchableOpacity style={{ width: '20%', justifyContent: 'center' }}>
                        <Text style={{ fontFamily: fonts.REGULAR, color: '#000', fontSize: scale(13) }}>{t('Google')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ width: '20%', justifyContent: 'center' }}>
                        <Text style={{ fontFamily: fonts.REGULAR, color: '#000', fontSize: scale(13) }}>{t('In-App')}</Text>
                    </TouchableOpacity>
                    <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center', }}>
                        <Ionicons name={"chevron-forward"} size={20} color={'#888889'} />
                    </View>
                </TouchableOpacity>
                <CardNavigationBtn type="icon"
                    image={<Ionicons name="analytics-sharp" size={25} color={Colors.primary} />}
                    title="Route Preference"
                    onPressButton={() => { }} />
            </View>

            <View style={{ borderBottomWidth: 1, marginBottom: 10, borderColor: '#dcdcdc' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', paddingVertical: 10, fontSize: scale(14) }}>{t('Notification Settings')}</Text>
                <CardNavigationBtn type="icon"
                    image={<Ionicons name="volume-high-outline" size={25} color={Colors.primary} />}
                    title="Sound"
                    onPressButton={() => { }} />
                <CardNavigationBtn type="icon"
                    image={<MaterialCommunityIcons name="vibrate" size={25} color={Colors.primary} />}
                    title="Vibration"
                    onPressButton={() => { }} />
            </View>

            <View style={{ borderBottomWidth: 1, marginBottom: 10, borderColor: '#dcdcdc' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', paddingVertical: 10, fontSize: scale(14) }}>{t('Accessibility')}</Text>
                <CardNavigationBtn type="icon"
                    image={<MaterialCommunityIcons name="format-size" size={25} color={Colors.primary} />}
                    title="Text size"
                    onPressButton={() => { }} />
                <CardNavigationBtn type="icon"
                    image={<Ionicons name="reader-outline" size={25} color={Colors.primary} />}
                    title="Read instructions on arrival"
                    onPressButton={() => { }} />
            </View>

            <View style={{ borderBottomWidth: 1, marginBottom: 10, borderColor: '#dcdcdc' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', paddingVertical: 10, fontSize: scale(14) }}>{t('Language')}</Text>
                <CardNavigationBtn type="icon"
                    image={<Ionicons name="language" size={25} color={Colors.primary} />}
                    title="Select App Language"
                    onPressButton={() => props.navigation.navigate('SelectLang')} />
            </View>

            <View style={{ borderBottomWidth: 1, marginBottom: 10, borderColor: '#dcdcdc' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', paddingVertical: 10, fontSize: scale(14) }}>{t('Security and Privacy')}</Text>
                <CardNavigationBtn type="icon"
                    image={<Ionicons name="reader-outline" size={25} color={Colors.primary} />}
                    title="Privacy Settings"
                    onPressButton={() => { }} />
                <CardNavigationBtn type="icon"
                    image={<MaterialCommunityIcons name="passport-biometric" size={25} color={Colors.primary} />}
                    title="Passcode/Biometric"
                    onPressButton={() => { }} />
            </View>

            <View style={{ borderBottomWidth: 1, marginBottom: 10, borderColor: '#dcdcdc' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', paddingVertical: 10, fontSize: scale(14) }}>{t('Support and Help')}</Text>
                <CardNavigationBtn type="icon"
                    image={<MaterialCommunityIcons name="chat-question-outline" size={25} color={Colors.primary} />}
                    title="FAQs"
                    onPressButton={() => { }} />
                <CardNavigationBtn type="icon"
                    image={<MaterialIcons name="support-agent" size={25} color={Colors.primary} />}
                    title="Contact Support"
                    onPressButton={() => { }} />
                <CardNavigationBtn type="icon"
                    image={<MaterialIcons name="report-problem" size={25} color={Colors.primary} />}
                    title="Report a problem"
                    onPressButton={() => { }} />
                <CardNavigationBtn type="icon"
                    image={<MaterialIcons name="emergency" size={25} color={Colors.primary} />}
                    title="Emergency Assistance"
                    onPressButton={() => { }} />
            </View>
            <View style={{ height: 30, backgroundColor: '#fefefe', alignItems: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: fonts.REGULAR, color: '#888889', fontSize: scale(10) }}>QR Cabs Version 1.0.0</Text>
            </View>
        </ScrollView>
    )
}
export default Settings