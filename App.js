import 'react-native-gesture-handler';
import * as React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react'
import { LanguageProvider } from './src/languages/LanguageContext';

import Root from './src/navigators/Navigation';
import { Store, persistor } from './src/Redux/Reducer'

function App() {
  return (

    <Provider store={Store}>
      <PersistGate loading={null} persistor={persistor}>
        <LanguageProvider>

          <Root />
        </LanguageProvider>

      </PersistGate>
    </Provider>

  );
}
export default App