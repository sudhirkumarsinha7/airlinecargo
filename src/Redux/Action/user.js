import axios from 'axios';

import {config, getIosDeviceTokeApi,auth} from '../../config/config';
import {
 COLLECTION,
 ON_SALE,
 FAV,
 PURCHASED,
 UNLISTED,
 WALLET_ADDRESS
} from '../constant';

import {loadingOff, loadingOn} from './loader';
export const setWalletAddress =(Add)=>async(dispatch,getState) =>{
    dispatch({
        type: WALLET_ADDRESS,
        payload: Add,
      });
}
export const getUserCreatedList = (id) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('Action_ggetUserCreatedList' + config().userCreatedDetails);
  
      const response = await axios.get(config().userCreatedDetails);
      console.log('Action_getUserCreatedList_response' + JSON.stringify(response));
      dispatch({
        type: ON_SALE,
        payload: response.data.data,
      });
  
      return response;
    } catch (e) {
      console.log('Action_getUserCreatedList_error' + JSON.stringify(e));
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getUserPurchaseList = (id) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('Action_ggetUserPurchaseList' + config().userPerchageDetails);
  
      const response = await axios.get(config().userPerchageDetails);
      console.log('Action_getUserPurchaseList_response' + JSON.stringify(response));
      dispatch({
        type: PURCHASED,
        payload: response.data.data,
      });
  
      return response;
    } catch (e) {
      console.log('Action_getUserPurchaseList_error' + JSON.stringify(e));
    } finally {
      loadingOff(dispatch);
    }
  };

  export const getUserUnlisted = (id) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('Action_ggetUserUnlisted' + config().userUnlistedDetails);
  
      const response = await axios.get(config().userUnlistedDetails);
      console.log('Action_getUserUnlisted_response' + JSON.stringify(response));
      dispatch({
        type: UNLISTED,
        payload: response.data.data,
      });
  
      return response;
    } catch (e) {
      console.log('Action_getUserUnlisted_error' + JSON.stringify(e));
      console.log('Action_getUserUnlisted message' + JSON.stringify(e.message));

    } finally {
      loadingOff(dispatch);
    }
  };


  export const getUserFavorites = (id) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('Action_ggetUserFavorites' + config().userFavDetails);
  
      const response = await axios.get(config().userFavDetails);
      console.log('Action_getUserFavorites_response' + JSON.stringify(response));
      dispatch({
        type: FAV,
        payload: response.data.data,
      });
  
      return response;
    } catch (e) {
      console.log('Action_getUserFavorites_error' + JSON.stringify(e));
    } finally {
      loadingOff(dispatch);
    }
  };
  export const getUserCollection = (id) => async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      console.log('Action_ggetUserCollection' + config().userCollectionDetails);
  
      const response = await axios.get(config().userCollectionDetails);
      console.log('Action_getUserCollection_response' + JSON.stringify(response));
      dispatch({
        type: COLLECTION,
        payload: response.data.data,
      });
  
      return response;
    } catch (e) {
      console.log('Action_getUserCollection_error' + JSON.stringify(e));
    } finally {
      loadingOff(dispatch);
    }
  };