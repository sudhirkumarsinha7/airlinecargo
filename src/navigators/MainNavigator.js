import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import Splash from '../screens/Splash';
import Login from '../screens/Login';
import InitialScreen from '../screens/Login/initialScreen';

import Register from '../screens/Register';
import AuthLoadingScreen from '../screens/AuthLoadingScreen'
import BottomTabNavigator from './BottomTabNavigator';
import VerifyOTP from '../screens/Login/VerifyOTP';
import SelectLang from '../screens/Settings/SelectLang';

const Stack = createNativeStackNavigator();
const headerOptions = {
  headerShown: false,
};
const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={headerOptions}>
      {/* <Stack.Screen name="initial" component={InitialScreen} /> */}

      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="VerifyOTP" component={VerifyOTP} />
      <Stack.Screen name="register" component={Register} />
    </Stack.Navigator>
  );
};
export default function MainNavigator() {
  const headerOptions = {
    headerShown: false
  };
  return (
    <Stack.Navigator screenOptions={headerOptions} headerMode="float">
      {/* <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Auth" component={AuthStack} />
      <Stack.Screen name="AuthLoading" component={AuthLoadingScreen} /> */}
      <Stack.Screen name="main" component={BottomTabNavigator} />

      <Stack.Screen name="SelectLang" component={SelectLang} />

    </Stack.Navigator>
  );
}
