import _ from "lodash";
import React, { useState, useEffect, useRef } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    StyleSheet,
    Animated,
    TextInput
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import fonts from "../../constants/fonts";
import { scale } from "../Scale";
import { Colors } from "../Common/Style";

const DropDown = ({
    setSelected,
    placeholder,
    dropdownStyles,
    dropdownItemStyles,
    maxHeight,
    data,
    keyitem,
    defaultOption,
    searchicon = false,
    arrowicon = false,
    search = true,
    searchPlaceholder = "search",
    onSelect = () => { },
}) => {
    const oldOption = useRef(null)
    const [_firstRender, _setFirstRender] = useState(true);
    const [dropdown, setDropdown] = useState(false);
    const [selectedval, setSelectedVal] = useState("");
    const [height, setHeight] = useState(200)
    const animatedvalue = useRef(new Animated.Value(0)).current;
    const [filtereddata, setFilteredData] = useState(data)

    const slidedown = () => {
        setDropdown(true)
        Animated.timing(animatedvalue, {
            toValue: height,
            duration: 500,
            useNativeDriver: false,

        }).start()
    }

    const slideup = () => {

        Animated.timing(animatedvalue, {
            toValue: 0,
            duration: 500,
            useNativeDriver: false,

        }).start(() => setDropdown(false))
    }

    useEffect(() => {
        if (maxHeight)
            setHeight(maxHeight)
    }, [maxHeight])

    useEffect(() => {
        setFilteredData(data);
    }, [data])

    useEffect(() => {
        if (_firstRender) {
            _setFirstRender(false);
            return;
        }
        onSelect()
    }, [selectedval])

    useEffect(() => {
        if (!_firstRender && defaultOption
            // && oldOption.current != defaultOption.key 
            && oldOption.current != null) {
            oldOption.current = defaultOption
            setSelected(defaultOption);
            setSelectedVal(defaultOption);
        }
        if (defaultOption && _firstRender
            // && defaultOption.key != undefined
        ) {
            oldOption.current = defaultOption
            setSelected(defaultOption);
            setSelectedVal(defaultOption);
        }
    }, [defaultOption]);

    return (
        <View>
            {
                (dropdown && search)
                    ?
                    <View style={[styles.wrapper]}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                            {
                                (!searchicon)
                                    ?
                                    <Ionicons
                                        name="ios-search"
                                        size={20}
                                        color="#000"
                                        style={{ marginRight: 7 }}
                                    />
                                    :
                                    searchicon
                            }

                            <TextInput
                                placeholderTextColor={'#AEAEAE'}
                                placeholder={searchPlaceholder}
                                onChangeText={(val) => {
                                    let result = data.filter((item) => {
                                        val.toLowerCase();
                                        // let row = keyitem.toLowerCase()
                                        let row = _.get(item, keyitem).toLowerCase()
                                        return row.search(val.toLowerCase()) > -1;
                                    });
                                    setFilteredData(result)
                                    // setFilteredData(val)
                                }}
                                style={[{ padding: 0, height: 20, flex: 1, color: selectedval == "" ? '#566D80' : Colors.black0, fontSize: scale(14), fontFamily: fonts.REGULAR }]}
                            />
                            <TouchableOpacity onPress={() => slideup()} >
                                <Ionicons
                                    name="ios-chevron-up"
                                    size={20}
                                    color="#000"
                                    style={{ paddingLeft: 7 }}
                                />
                            </TouchableOpacity>
                        </View>

                    </View>
                    :
                    <TouchableOpacity style={[styles.wrapper]} onPress={() => { if (!dropdown) { slidedown() } else { slideup() } }}>
                        <Text style={{
                            fontFamily: fonts.REGULAR, fontSize: 16,
                            //  color: '#dc143c'
                            color: (selectedval == "") ? '#AEAEAE' : Colors.black0
                        }}>{(selectedval == "") ? (placeholder) ? placeholder : 'Select option' : (keyitem == "data" ? (`${selectedval}`) : selectedval)}</Text>
                        {
                            (!arrowicon)
                                ?
                                <Ionicons
                                    name="chevron-down"
                                    size={20}
                                    color="#000"
                                    style={{ alignSelf: 'center' }}
                                />

                                :
                                arrowicon
                        }

                    </TouchableOpacity>
            }

            {
                (dropdown)
                    ?
                    <Animated.View style={{ maxHeight: animatedvalue }}>
                        <ScrollView style={[styles.dropdown, dropdownStyles]} contentContainerStyle={{ paddingVertical: 10, }} nestedScrollEnabled={true}>

                            {
                                (filtereddata.length >= 1)
                                    ?
                                    filtereddata.map((item, index) => {
                                        // let key = item.key ?? item.name ?? item;
                                        let key = item.key ?? _.get(item, keyitem) ?? item;
                                        let value = _.get(item, keyitem) ?? item;
                                        return (
                                            <TouchableOpacity style={[styles.option, dropdownItemStyles]} key={index} onPress={() => {
                                                setSelected(key)
                                                setSelectedVal(value)
                                                slideup()
                                                setTimeout(() => setFilteredData(data), 800)

                                            }}>
                                                <Text style={styles.dropdownTextStyles}>{keyitem == "salary" ? (`${value}`) : value}</Text>
                                            </TouchableOpacity>
                                        )
                                    })
                                    :
                                    <TouchableOpacity style={[styles.option, dropdownItemStyles]} onPress={() => {
                                        setSelected("")
                                        setSelectedVal("")
                                        slideup()
                                        setTimeout(() => setFilteredData(data), 800)

                                    }}>
                                        <Text style={styles.dropdownTextStyles}> No data found</Text>
                                    </TouchableOpacity>
                            }



                        </ScrollView>
                    </Animated.View>
                    :
                    null
            }


        </View>
    )
}
export default DropDown;

const styles = StyleSheet.create({
    wrapper: {
        borderWidth: 0.6,
        borderRadius: 6,
        borderColor: "#0000001A",
        paddingHorizontal: 20,
        paddingVertical: 12,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    dropdown: {
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#0000001A',
        marginTop: 10
    },
    option: {
        paddingHorizontal: 20,
        paddingVertical: 8
    },
    inputStyles: {
        fontFamily: fonts.REGULAR,
        fontSize: 18,
        color: '#808080'
    },
    dropdownTextStyles: {
        fontFamily: fonts.REGULAR,
        color: '#000',
        fontSize: 17
    }
})