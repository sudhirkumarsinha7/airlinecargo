import React, { useEffect } from "react";
import { View, Text, Image, StatusBar } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import fonts from "../../constants/fonts";
import { scale } from "../../Components/Scale";
import { useTranslation } from 'react-i18next';

const Success = (props) => {
    const { t, i18n } = useTranslation();

    async function navigaeScreen() {
        setTimeout(() => {
            props.navigation.navigate('main')
        }, 2000);
    }
    useEffect(() => {
        navigaeScreen();
    }, []);

    useEffect(() => {
        const unsubscribe = props.navigation.addListener("focus", () => {
            navigaeScreen()
        });
        return unsubscribe;
    }, [props.navigation]);


    return (
        <View style={{ flex: 1 }}>
            <StatusBar hidden={false} backgroundColor={'#f2edf9'} barStyle={'dark-content'} />
            <LinearGradient
                locations={[0.1, 0.5, 0.9]}
                colors={[
                    '#f2edf9',
                    '#cfbeeb',
                    '#b096df',
                ]}
                style={{ flex: 1 }}>
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'space-evenly' }}>
                    <Text style={{ color: '#000', fontFamily: fonts.MEDIUM, fontSize: scale(13), textAlign: 'center' }}>{t('We are setting up your account please wait for a moment....')}</Text>
                </View>
            </LinearGradient>
        </View>
    )
}

export default Success;