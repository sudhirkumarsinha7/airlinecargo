export default {
    BLACK: 'Inter-Black',
    BOLD: 'Inter-Bold',
    LIGHT: 'Inter-Light',
    MEDIUM: 'Inter-Medium',
    REGULAR: 'Inter-Regular',
    SEMI_BOLD: 'Inter-SemiBold'
  };