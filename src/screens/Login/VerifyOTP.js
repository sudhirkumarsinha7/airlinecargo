import React, { useRef, useState } from 'react';
import {
  Alert,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { CustomButton } from '../../Components/Common/Button';
import { SafeAreaView } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
// import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { scale } from '../../Components/Scale';
import fonts from '../../constants/fonts';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const VerifyOTP = props => {
  const { t, i18n } = useTranslation();
  const phone = props.route.params.phone;
  const ref1 = useRef([]);
  const insets = useSafeAreaInsets();

  const [digitFocus, setDigitFocus] = useState([true, false, false, false]);
  const [otpCode, setOtpCode] = useState(0);
  const [errorMessage, setErrorMessage] = useState('');

  function handleOTPsubmit(fIndex, digit) {
    let digitFocusArr = [...digitFocus]; // Create a new copy of the digitFocus array
    let updatedOTP = otpCode; // Initialize the updatedOTP with the current otpCode

    // Check if the entered digit is empty (user erased the input)
    if (digit === '') {
      // If the user erased a digit, set the focus on the previous input
      if (fIndex > 0) {
        ref1.current[fIndex - 1].focus();
      }
      // Reset the current digit to be focused
      digitFocusArr[fIndex] = true;
      // Remove the last digit from the updatedOTP
      updatedOTP = Math.floor(updatedOTP / 10);
    } else {
      // If the user entered a digit, proceed as before

      if (fIndex === 3) {
        // If the last digit is entered, do not focus on the next input
        ref1.current[3].blur();
        // To dismiss the keyboard after the last digit
      } else {
        ref1.current[fIndex + 1].focus();
      }

      // Update the digit in the digitFocusArr
      digitFocusArr[fIndex] = false;

      // Concatenate the digit with the previous OTP code
      updatedOTP = updatedOTP * 10 + Number(digit);
    }

    // Update the otpCode state with the new OTP
    setOtpCode(updatedOTP);
    // setCode(updatedOTP);
    // Set the digit focus array to the state
    setDigitFocus(digitFocusArr);
  }

  function resendOTP() {
    console.log('otp resent to your number');
  }

  function verifyOTP() {
    // if (otpCode === 3434) {
    //   setErrorMessage('');
    //   props.navigation.navigate('ProfileDetail');
    // } else {
    //   setErrorMessage('Invalid Code Try again');
    // }
    props.navigation.navigate('ProfileDetail');
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: insets.top,
        paddingBottom: insets.bottom
      }}>
      <StatusBar
        hidden={false}
        backgroundColor={'#fff'}
        barStyle={'dark-content'}
      />
      <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={styles.backBtn}>
        <Ionicons name="chevron-back-outline" size={25} color={'black'} />
      </TouchableOpacity>
      <View style={styles.container}>
        <Text style={styles.title}>{t('Enter OTP')}</Text>
        <Text style={styles.subTitle}>{t('OTP Description') + ' ' + phone}</Text>

        <View style={styles.inputWrapper}>
          {[0, 1, 2, 3].map((item, index) => (
            <TextInput
              key={index}
              ref={element => (ref1.current[index] = element)}
              style={[
                styles.input,
                { borderBottomColor: errorMessage != '' ? '#FF5E56' : '#B8B8B8' },
              ]}
              returnKeyType={'next'}
              keyboardType="number-pad"
              maxLength={1}
              placeholder=""
              autoFocus={digitFocus[index]}
              onChangeText={text => handleOTPsubmit(index, text)}
            />
          ))}
        </View>
        <Text style={styles.error}>{errorMessage && errorMessage}</Text>

        <View style={styles.bottomText}>
          <Text style={[styles.subTitle, { alignSelf: 'center' }]}>30s</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              // alignItems: 'center',
            }}>
            <Text style={{ fontSize: scale(13), fontFamily: fonts.REGULAR, color: '#000' }}>{t('Didnt get the code ?')}</Text>
            <TouchableOpacity>
              <Text style={{ fontSize: scale(13), fontFamily: fonts.REGULAR, color: '#470AB4' }}>
                {t('Resend')}
              </Text>
            </TouchableOpacity>
          </View>
          <CustomButton
            bgColor={otpCode.toString().length == 4 ? '#470AB4' : '#A384D9'}
            disabled={otpCode.toString().length != 4}
            buttonName={t('Next')}
            navigation={props.navigation}
            onPressButton={verifyOTP}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, paddingHorizontal: 26, paddingTop: 10 },
  backBtn: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#E5E7EB',
  },
  title: { fontSize: scale(20), fontFamily: fonts.SEMI_BOLD, color: '#090A0A' },
  subTitle: {
    fontSize: scale(14),
    fontFamily: fonts.REGULAR,
    // fontWeight: '500',
    color: '#090A0A',
    lineHeight: 25,
    marginVertical: 10,
  },
  inputWrapper: {
    paddingTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  input: {
    color: '#000',
    fontFamily: fonts.REGULAR,
    padding: 10,
    paddingHorizontal: 25,
    borderBottomWidth: 1,
    fontSize: scale(19),
    borderRadius: 6,
  },
  error: { color: '#FF5E56', marginTop: 10 },
  bottomText: { flex: 1, justifyContent: 'flex-end' },
});

export default VerifyOTP;
