import {View, Text, StyleSheet} from 'react-native';
import React from 'react';

const Avatar = ({avatarIcon}) => {
  return <View style={styles.container}>{avatarIcon && avatarIcon}</View>;
};

const styles = StyleSheet.create({
  container: {
    width: 82,
    height: 82,
    backgroundColor: '#FFFFFF',
    borderRadius: 41,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#00000014',
    borderWidth: 1,
  },
});

export default Avatar;
