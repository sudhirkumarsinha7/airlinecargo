import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import React from 'react';
import { Image, Platform, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { useTheme } from '../theme/Hooks'

import { Colors } from "../Components/Common/Style";
import { scale } from '../Components/Scale';
import fonts from '../constants/fonts';
import { useTranslation } from 'react-i18next';
import { HomeStack } from '../screens/Home/stack';

// const BottomTabNavigator = ({ navigation, route }) => {
//   const { t, i18n } = useTranslation();

//   const Tab = createBottomTabNavigator();
//   const { darkMode, NavigationTheme } = useTheme();
//   const { colors } = NavigationTheme
//   return (
//     <Tab.Navigator
//       screenOptions={{
//         headerShown: false,
//         tabBarStyle: {
//           backgroundColor: 'transparent',
//           height: 80
//         }
//       }}
//     >
//       <Tab.Screen name="main" component={HomeStack} options={{
//         tabBarLabel: "",
//         tabBarIcon: ({ focused }) => (
//           <View style={{ height: '100%', marginTop: 10 }}>

//             <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 10, alignSelf: 'center', color: focused ? Colors.primary : '#888889', marginTop: 3 }}>{t('Home')}</Text>
//           </View>
//         ),
//         tabBarIconStyle: {
//           width: '100%',
//         }
//       }}
//       ></Tab.Screen>

//     </Tab.Navigator>
//   );
// };
const Stack = createNativeStackNavigator();
const headerOptions = {
  headerShown: false,
};
const BottomTabNavigator = ({ navigation, route }) => {
  const { t, i18n } = useTranslation();

  const Tab = createBottomTabNavigator();
  const { darkMode, NavigationTheme } = useTheme();
  const { colors } = NavigationTheme

  return (
    <Stack.Navigator screenOptions={headerOptions}>

      <Stack.Screen name="main" component={HomeStack} />

    </Stack.Navigator>)
};
export default BottomTabNavigator;
const styles = StyleSheet.create({});
