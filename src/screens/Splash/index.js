import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ImageBackground, Image, StatusBar, Animated } from 'react-native';
import { scale, verticalScale } from '../../Components/Scale';
import DeviceInfo from 'react-native-device-info';
import LinearGradient from "react-native-linear-gradient";
import fonts from '../../constants/fonts';
import { localImage } from '../../config/global';

const ImageLoader = (props) => {
    const opacity = new Animated.Value(0)

    const onLoad = () => {
        Animated.timing(opacity, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }).start();
    };

    return (
        <Animated.Image
            onLoad={onLoad}
            {...props}
            style={[
                {
                    opacity: opacity,
                    transform: [
                        {
                            scale: opacity.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0.85, 1],
                            }),
                        },
                    ],
                },
                props.style,
            ]}
        />
    );
}

const Splash = (props) => {
    const [deviceVersion, setDeviceversion] = useState()

    useEffect(() => {
        async function getData() {
            const deviceid = await DeviceInfo.getVersion();
            setDeviceversion(deviceid)
            setTimeout(() => {
                props.navigation.navigate('Auth')
            }, 200);
        }
        getData()
    }, []);

    return (
        <ImageBackground
            source={localImage.backgraound}
            resizeMode="cover"
            style={styles.imgBackground}
        >

            <View style={styles.container}>
                <View style={styles.container}>
                    <ImageLoader
                        style={{ width: scale(150), height: scale(150), marginBottom: '60%' }}
                        source={localImage.logo}
                        resizeMode="contain"
                    />

                    <Text
                        style={{
                            fontSize: scale(20),
                            color: 'white',
                        }}>
                        Airline Portal
                    </Text>
                    <View style={{ alignItems: 'center', marginBottom: '20%', marginTop: '20%' }}>
                        <View style={{ alignItems: 'center' }}>
                            <Text
                                style={{
                                    fontSize: scale(12),
                                    color: 'white',
                                }}>
                                Powered By
                            </Text>
                            <Image
                                style={{
                                    width: scale(120),
                                    height: scale(24),
                                    borderWidth: 0,
                                    marginTop: verticalScale(8),
                                }}
                                source={localImage.STATWIG}
                                resizeMode="contain"
                            />

                        </View>
                        <Text
                            style={{
                                color: 'white',
                                fontFamily: fonts.MEDIUM,
                                fontSize: scale(14),
                                marginTop: verticalScale(8),
                            }}>
                            Version {deviceVersion}
                        </Text>
                    </View>
                </View>
            </View>
        </ImageBackground>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
});
export default Splash;


