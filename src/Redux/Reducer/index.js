import AsyncStorage from '@react-native-async-storage/async-storage'
import { configureStore, combineReducers } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import {
  persistReducer,
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'
import auth from './auth';
import home from './home';
import { api } from '../Services/api'
import theme from './slice.reducer'

const reducers = combineReducers({
  theme,
  api: api.reducer,
  home,
  auth: auth,
  // user:persistReducer(persistConfigUser, user),
})
const persistConfigAuth = {
  key: 'auth',
  storage: AsyncStorage,
  whitelist: ['userinfo'],
}
const persistConfigUser = {
  key: 'user',
  storage: AsyncStorage,
  whitelist: ['walletAddress'],
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['theme'],
}

const persistedReducer = persistReducer(persistConfig, reducers)

const Store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware => {
    const middlewares = getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(api.middleware)

    // if (__DEV__ && !process.env.JEST_WORKER_ID) {
    //   const createDebugger = require('redux-flipper').default
    //   middlewares.push(createDebugger())
    // }

    return middlewares
  },
})

const persistor = persistStore(Store)

setupListeners(Store.dispatch)

export { Store, persistor }
