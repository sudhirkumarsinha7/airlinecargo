import React, { useRef, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Alert,
    TouchableOpacity,
    StatusBar,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { CustomTextView } from '../../Components/Common/heper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { scale } from '../../Components/Scale';
import fonts from '../../constants/fonts';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Colors } from '../../Components/Common/Style';
import { CustomInputText, SubHeaderComponent, CustomTextLine } from "../../Components/Common/heper"
import { Header1 } from '../../Components/Header'
import { CustomButton, CustomButtonWithBorder } from '../../Components/Common/Button';

const Home = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const insets = useSafeAreaInsets();

    return (
        <View
            style={{
                flex: 1,

            }}>
            <StatusBar
                hidden={false}
                backgroundColor={'#fff'}
                barStyle={'dark-content'}
            />
            <Header1 isBack={true} name={'Shipment Details'} navigation={props.navigation} textColor={'#fff'} />

            <View style={{ backgroundColor: Colors.whiteFF, margin: 15, padding: 15, borderRadius: 10 }}>
                <SubHeaderComponent
                    name='Flight Details'
                    textColor='#FF6A28' />
                <CustomTextView
                    leftText={'Airline'}
                    rightText={'Indigo'}
                />
                <CustomTextView
                    leftText={'Flight'}
                    rightText={'9E-8976'}
                />
                <CustomTextView
                    leftText={'Flight Date'}
                    rightText={'06/12/2023'}
                />
                <CustomTextView
                    leftText={'Flight Time'}
                    rightText={'8:00 PM'}
                />

                <SubHeaderComponent
                    name='Shipment Billing'
                    textColor='#FF6A28' />

                <Text style={styles.title}>{'Total Bill Details'}</Text>
                <CustomTextView
                    leftText={'Net Amount'}
                    rightText={'₹ 2,000'}
                />
                <CustomTextView
                    leftText={'Discount Applied'}
                    rightText={'- ₹ 500'}
                    rightTextColor={'#56BD4D'}
                    leftTextColor={'#56BD4D'}
                />
                <CustomTextView
                    leftText={'Taxes (GST)'}
                    rightText={'₹ 100'}
                />

                <CustomTextView
                    leftText={'Total amount'}
                    rightText={'₹ 1,600'}
                    rightTextColor={'#1C1C1C'}

                />
                <SubHeaderComponent
                    name='You saved ₹ 500 on this order! '
                    textColor='#000' />

                <Text style={styles.subTitle}>{'Please verify all the above information and proceed with the payment options'}</Text>

                <View style={{ flexDirection: 'row', }}>
                    <View style={{ flex: 0.4 }}>

                        <CustomButtonWithBorder
                            bdColor={'#E3E5E5'}
                            buttonName={t('Cancel')}
                            navigation={props.navigation}
                            onPressButton={() => props.navigation.goBack()}
                        />
                    </View>

                    <View style={{ flex: 0.6 }}>

                        <CustomButton
                            bgColor={'#FF6A28'}
                            buttonName={t('Make Payment')}
                            navigation={props.navigation}
                            onPressButton={() => props.navigation.navigate('payment')}
                        />
                    </View>

                </View>
            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: { flex: 1, paddingHorizontal: 17, paddingVertical: 10 },
    title: { fontSize: scale(16), fontFamily: fonts.SEMI_BOLD, color: '#090A0A', marginLeft: 10 },
    subTitle: {
        fontSize: scale(13),
        fontFamily: fonts.REGULAR,
        color: '#888889',
        marginVertical: 10,
    },
    phoneInputContainer: {
        width: '100%',
        overflow: 'visible',
        borderWidth: 1,
        borderRadius: 1,
        overflow: 'visible',
        borderColor: '#a8a8a8',
        // marginBottom: 5,
    },
    phoneInputTextContainer: {
        // overflow: 'visible',
        backgroundColor: '#FFFFFF',
    },
    phoneInputText: { padding: 0 },
});
export default Home;
