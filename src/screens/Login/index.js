import React, { useRef, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Alert,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
  Keyboard
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { CustomButton, CustomButtonLine } from '../../Components/Common/Button';
import { scale } from '../../Components/Scale';
import fonts from '../../constants/fonts';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Colors } from '../../Components/Common/Style';
import { DeviceWidth, localImage } from '../../config/global';
import { CustomInputText, SubHeaderComponent, CustomTextLine } from "../../Components/Common/heper"
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Header1 } from '../../Components/Header'

const Login = props => {
  const { t, i18n } = useTranslation();
  const isFocused = useIsFocused();
  const insets = useSafeAreaInsets();
  const [isMandatory, setIsMandatory] = useState(false)
  const [errorMessage, setErrorMsg] = useState('')
  var validationSchema = Yup.object().shape(
    {
      phoneNumber: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Required')),
      otp: Yup.string()
        .min(1, t('Too Short!'))
        .max(256, t('Too Lonng!'))
        .required(t('Required')),

    },
    ['phoneNumber',],
  ); // <-- HERE!!!!!!!!
  const signinform = useFormik({
    initialValues: {
      phoneNumber: '',
      otp: ''

    },
    validationSchema,
    onSubmit: (values, actions) => {
      handleSubmit({ ...values });
    },
  });
  const _signin = () => {
    Keyboard.dismiss();
    setIsMandatory(true)
    signinform.handleSubmit();
  };

  const handleSubmit = async values => {
    props.navigation.navigate('main')

  };

  return (
    <ImageBackground
      source={localImage.backgraound}
      resizeMode="cover"
      style={styles.imgBackground}
    >
      <View
        style={{
          flex: 1,
          paddingTop: insets.top + 20,
          paddingBottom: insets.bottom,
        }}>
        <StatusBar
          hidden={false}
          backgroundColor={'#fff'}
          barStyle={'dark-content'}
        />
        <View >
          <View style={{ alignItems: 'center', marginTop: '15%' }}>
            <Text style={styles.title}>{'Login to Account'}</Text>
          </View>

          <View style={{ margin: scale(15), padding: 15, backgroundColor: '#fff', borderRadius: 15, justifyContent: 'center', }}>

            <CustomInputText
              label={'Phone Number '}
              placeholderText={'Phone Number '}
              val={signinform?.values?.phoneNumber}
              onChange={(text) => { signinform.handleChange({ target: { name: `phoneNumber`, value: text, } }) }}
              errorMsg={signinform?.errors?.phoneNumber}
              mandatory={isMandatory}

            />
            <CustomInputText
              label={'otp '}
              placeholderText={'Enter otp'}
              val={signinform?.values?.otp}
              onChange={(text) => { signinform.handleChange({ target: { name: `otp`, value: text, } }) }}
              errorMsg={signinform?.errors?.otp}
              mandatory={isMandatory}

            />
            {errorMessage ? <SubHeaderComponent
              name={errorMessage}
              textColor={'red'}
            /> : null}
            <View style={{ marginTop: 20 }} />
            <CustomButton
              bgColor={'#FF6A28'}
              buttonName={t('Verify')}
              navigation={props.navigation}
              onPressButton={_signin}
            />

            <View style={{ flexDirection: 'row', alignItems: 'center', padding: scale(15), justifyContent: 'center' }}>
              <CustomTextLine
                name={'Forgot PIN? '}
                textColor={'#000'}
              />
              <CustomButtonLine buttonName={'Reset PIN'} onPressButton={_signin} bgColor={'#FF6A28'} />

            </View>
          </View>






        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, paddingHorizontal: 17, paddingVertical: 10 },
  title: { fontSize: scale(24), fontFamily: fonts.BOLD, color: '#000', fontWeight: '900' },
  subTitle: {
    fontSize: scale(13),
    fontFamily: fonts.REGULAR,
    color: '#888889',
    marginVertical: 10,
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  formContainer: {
    width: '100%',
  },
});
export default Login;
