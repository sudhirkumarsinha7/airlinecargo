/* eslint-disable react-native/no-color-literals */
/* eslint-disable sort-keys */
import { Dimensions, Platform, StyleSheet } from 'react-native';

import { moderateScale, scale, verticalScale } from '../Scale';
// @flow
export const DeviceHeight = Dimensions.get('window').height;
export const DeviceWidth = Dimensions.get('window').width;

export const { width } = Dimensions.get('window');
export const Colors = {
  black0: '#000000',
  black70: '#707070',
  blueFF: '#00B2FF',
  blueDA: '#00B5DA',
  whiteFF: '#FFFFFF',
  gray: '#AFAFAF',
  red00: '#FF0000',
  backgroundBlue: '#003978',
  cardBlue: '#001F5E',
  grayB1: '#B1B1B1',
  yellow3B: '#FFA83B',
  leftgray: '#707070',
  rightBlack: '#000000',
  whiteF7: '#F7F7F7',
  whiteF2: '#F2F2F2',
  orange: '#FFA83B',
  blueFc:'#1A83FC',
  primary: '#470AB4'
}
const color = '#157efb';
const ICON_COLOR = Platform.OS === 'ios' ? color : '#ffffff';

export const CommonStyle = StyleSheet.create({
  centeredPage: {
    alignContent: 'center',
    justifyContent: 'center',
  },
  dropdownView: {
    flexDirection: 'row',
    // height: scale(30),
    justifyContent: 'center',
    alignItems: 'center',
    margin: verticalScale(5),
    marginLeft: 10,
    marginRight: 10,
  },
  dropdownleftView: {
    width: '50%',
    // height: scale(35),
    // flexDirection: 'row',
    justifyContent: 'center',
  },
  dropdownRightView: {
    width: '55%',
    // height: scale(35),
    borderBottomWidth: 1,
    borderBottomColor: '#E8E8E8',
    alignItems: 'center',
    flexDirection: 'row',
  },
  blueCircle: {
    width: scale(20),
    height: scale(20),
    borderRadius: scale(10),
    backgroundColor: '#0172E6',
  },
  dotted: {
    width: scale(0.7),
    height: scale(3),
    borderRadius: scale(1),
    backgroundColor: '#0172E6',
    alignSelf: 'center',
    marginTop: scale(2),
  },
  labelStyleOrange: {
    marginLeft: Platform.OS === 'ios' ? 0 : 5, color: Colors.yellow3B, fontWeight: 'normal'
  },
  labelStylenormal: {
    fontWeight: '100', marginTop: -10
  }
});
export default {
  primaryColor: color,
  header: {
    backgroundColor: color,
    iconColor: ICON_COLOR,
  },
  input: {
    item: {
      borderColor: color,
      borderBottomWidth: 2,
      marginTop: 10,
    },
    label: {
      color: color,
    },
    picker: {
      ...Platform.select({
        android: {
          color: color,
        },
      }),
    },
  },
  form: {
    label: {
      color: color,
      fontSize: 15,
      includeFontPadding: false,
      marginTop: 10,
      marginBottom: 0,
      marginLeft: 15,
    },
    newlabel: {
      color: color,
      fontSize: 16,
      includeFontPadding: false,
      marginBottom: 0,
      // marginLeft: 2,
      // margin: 5,
    },
    labelUnit: {
      color: color,
      fontSize: 18,
      includeFontPadding: false,
      alignSelf: 'center',
      marginLeft: 10,
      marginRight: 10,
    },
    labelSection: {
      color: color,
      fontSize: 20,
      fontWeight: 'bold',
      marginTop: 10,
      marginBottom: 10,
      marginLeft: 5,
    },
    labelError: {
      color: 'red',
      fontSize: 12,
      marginTop: 0,
      marginBottom: 0,
      marginLeft: 15,
    },
    input: {
      borderColor: '#ccc',
      borderBottomWidth: 1,
      marginTop: 0,
      marginBottom: 0,
      marginLeft: 0,
    },
    inputRightIcon: {
      color: color,
      fontSize: 32,
      marginBottom: 8,
      position: 'absolute',
      right: 0,
      bottom: 0,
    },
    inputLeftIcon: {
      color: color,
      margin: 10,
    },
    inputError: {
      borderColor: 'red',
      borderBottomWidth: 2,
      marginTop: 0,
      marginBottom: 0,
      marginLeft: 15,
    },
  },
  centeredPage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  datePickerContainer: {
    borderBottomWidth: 2,
    borderBottomColor: color,
    marginTop: 25,
    width: width - 20,
  },
  pickerContainer: {
    borderBottomWidth: 2,
    borderBottomColor: color,
    marginTop: 25,
  },
  contentModal: {
    flexDirection: 'column',
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  buttonModal: {
    color: color,
    padding: 8,
  },
  inputModal: {
    borderColor: color,
    borderBottomWidth: 2,
  },
  filter: {
    content: {
      flex: 1,
      marginBottom: 10,
      marginHorizontal: 10,
    },
  },
};
