import { USERINFO, SELECTEDLANG } from '../constant';
const initialState = {
  userInfo: {},
  userLang: 'en'
};
export default function (state = initialState, action) {
  switch (action.type) {
    case SELECTEDLANG:
      return {
        ...state,
        userLang: action.payload,
      };
    case USERINFO:
      return {
        ...state,
        userInfo: action.payload,
      };
    default:
      return state;
  }
}
