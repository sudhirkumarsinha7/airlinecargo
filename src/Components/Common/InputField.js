import { View, Text, TextInput, StyleSheet } from 'react-native';
import React from 'react';
import { useTranslation } from 'react-i18next';


const InputField = ({
  label,
  placeholder,
  inputValue,
  setInputValue,
  inputStyle,
  labelStyle,
  rightIcon,
  secureTextEntry,
}) => {
  const { t, i18n } = useTranslation();

  return (
    <View>
      {label && <Text style={[styles.label, labelStyle]}>{t(label)}</Text>}
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TextInput
          placeholder={t(placeholder)}
          placeholderTextColor="#a8a8a8"
          secureTextEntry={secureTextEntry || false}
          onChangeText={setInputValue}
          value={inputValue}
          style={[styles.input, inputStyle]}
        />
        {rightIcon && rightIcon}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    backgroundColor: '#FFFFFF',
    borderColor: '#BCBCBC',
    borderWidth: 1,
    borderRadius: 12,
    height: 50,
    paddingHorizontal: 15,
    flex: 1,
  },
  label: { fontSize: 16, fontWeight: '500', color: '#090A0A' },
});
export default InputField;
