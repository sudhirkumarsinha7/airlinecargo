import React, { useEffect } from 'react';
import {
  Button,
  View,
  Text,
  StyleSheet,
  TextInput,
  ImageBackground,
  Image,
  BackHandler,
  ActivityIndicator,
  StatusBar,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import jwt_decode from 'jwt-decode';
import setAuthToken from '../../config/setAuthToken';
import { getUserInfo } from '../../Redux/Action/auth'
const AuthLoadingScreen = (props) => {
  const isFocused = useIsFocused();

  useEffect(() => {
    _loadData()
    // const backAction = () => {
    //   return true;
    // };
    // const backHandler = BackHandler.addEventListener(
    //   "hardwareBackPress",
    //   backAction
    // );
    // return () => backHandler.remove();
  }, [isFocused]);


  const _loadData = async () => {
    const token = await AsyncStorage.getItem('token');

    console.log('auth_tokenn', token);
    if (token) {
      const decoded = jwt_decode(token);
      console.log('decode' + JSON.stringify(decoded));
      const currenTime = Date.now() / 1000;
      if (decoded.exp < currenTime) {
        props.getUserInfo({})
        await AsyncStorage.clear();
        props.navigation.navigate('initial');
      } else {
        decoded.token = token
        props.getUserInfo(decoded)
        setAuthToken(token);
        props.navigation.navigate('main');;
      }
    } else {
      props.getUserInfo({})

      props.navigation.navigate('Auth');

    }
  };

  return (
    <View style={styles.container}>
      <ActivityIndicator />
      {/* <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center',}} /> */}
      <StatusBar barStyle="default" />
    </View>
  );

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

function mapStateToProps(state) {
  return {
    loder: state.loder,
  };
}

export default connect(mapStateToProps, { getUserInfo })(AuthLoadingScreen);
